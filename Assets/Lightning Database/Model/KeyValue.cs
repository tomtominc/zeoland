﻿namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class KeyValue
	{
		public string Key;
		public string Value;

		public KeyValue()
		{
		}

		public KeyValue(string key, string value)
		{
			Key = key;
			Value = value;
		}
	}
}
