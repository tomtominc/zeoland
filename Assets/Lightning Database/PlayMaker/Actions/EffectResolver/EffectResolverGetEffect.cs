using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverGetEffect : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("EffectResolver's ID")]
		public FsmInt id;
		
		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of EffectResolver's Effect.")]
		public FsmGameObject variable;

		public override void OnEnter()
		{
			EffectResolver item = database.Find(id.Value);
			if (item != null)
			{
				variable.Value = item.Effect;
			}
			Finish();
		}
	}
}
#endif
