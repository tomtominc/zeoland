using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverAdd : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;
		
		[HutongGames.PlayMaker.Tooltip("Name's value.")]
		public FsmString fieldName;
		
		[HutongGames.PlayMaker.Tooltip("Effect's value.")]
		public FsmGameObject fieldEffect;
		
		public override void OnEnter()
		{
			EffectResolver item = new EffectResolver();
			item.Name = fieldName.Value;
			item.Effect = fieldEffect.Value;
			
			database.Add(item);
			Finish();
		}
	}
}
#endif
