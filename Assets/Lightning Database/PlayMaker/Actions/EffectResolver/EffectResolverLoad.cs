using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverLoad : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Save slot index of EffectResolver")]
		public FsmInt saveSlot;

		public override void OnEnter()
		{
			database.Load(saveSlot.Value);
			Finish();
		}
	}
}
#endif
