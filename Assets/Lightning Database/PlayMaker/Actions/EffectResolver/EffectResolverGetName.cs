using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverGetName : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("EffectResolver's ID")]
		public FsmInt id;
		
		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of EffectResolver's Name.")]
		public FsmString variable;

		public override void OnEnter()
		{
			EffectResolver item = database.Find(id.Value);
			if (item != null)
			{
				variable.Value = item.Name;
			}
			Finish();
		}
	}
}
#endif
