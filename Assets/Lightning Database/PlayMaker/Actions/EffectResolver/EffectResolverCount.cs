using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverCount : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of EffectResolver count.")]
		public FsmInt variable;

		public override void OnEnter()
		{
			variable.Value = database.Count();
			Finish();
		}
	}
}
#endif
