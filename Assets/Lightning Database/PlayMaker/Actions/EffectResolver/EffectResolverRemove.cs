using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverRemove : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("EffectResolver's ID.")]
		public FsmInt id;

		public override void OnEnter()
		{
			database.Remove(id.Value);
			Finish();
		}
	}
}
#endif
