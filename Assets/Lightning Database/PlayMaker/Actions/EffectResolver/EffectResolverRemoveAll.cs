using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverRemoveAll : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		public override void OnEnter()
		{
			database.Clear();
			Finish();
		}
	}
}
#endif
