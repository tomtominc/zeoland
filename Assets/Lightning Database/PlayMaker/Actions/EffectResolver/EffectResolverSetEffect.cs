using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("EffectResolver (Lightning Database)")]
	public class EffectResolverSetEffect : FsmStateAction
	{
		[RequiredField]
		public EffectResolverDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("EffectResolver's ID")]
		public FsmInt id;
		
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of EffectResolver's Effect.")]
		public FsmGameObject value;

		public override void OnEnter()
		{
			EffectResolver item = database.Find(id.Value);
			if (item != null)
			{
				item.Effect = value.Value;
			}
			Finish();
		}
	}
}
#endif
