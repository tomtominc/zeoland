using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardRemove : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Card's ID.")]
		public FsmInt id;

		public override void OnEnter()
		{
			database.Remove(id.Value);
			Finish();
		}
	}
}
#endif
