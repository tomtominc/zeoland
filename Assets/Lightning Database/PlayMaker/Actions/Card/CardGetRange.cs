using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardGetRange : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Card's ID")]
		public FsmInt id;
		
		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Card's Range.")]
		public FsmInt variable;

		public override void OnEnter()
		{
			Card item = database.Find(id.Value);
			if (item != null)
			{
				variable.Value = item.Range;
			}
			Finish();
		}
	}
}
#endif
