using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardSetCost : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Card's ID")]
		public FsmInt id;
		
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Card's Cost.")]
		public FsmInt value;

		public override void OnEnter()
		{
			Card item = database.Find(id.Value);
			if (item != null)
			{
				item.Cost = value.Value;
			}
			Finish();
		}
	}
}
#endif
