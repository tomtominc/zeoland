using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardRemoveAll : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		public override void OnEnter()
		{
			database.Clear();
			Finish();
		}
	}
}
#endif
