using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardAdd : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;
		
		[HutongGames.PlayMaker.Tooltip("Name's value.")]
		public FsmString fieldName;
		
		[HutongGames.PlayMaker.Tooltip("LargeIcon's value.")]
		public FsmObject fieldLargeIcon;
		
		[HutongGames.PlayMaker.Tooltip("SmallIcon's value.")]
		public FsmObject fieldSmallIcon;
		
		[HutongGames.PlayMaker.Tooltip("MediumIcon's value.")]
		public FsmObject fieldMediumIcon;
		
		[HutongGames.PlayMaker.Tooltip("Cost's value.")]
		public FsmInt fieldCost;
		
		[HutongGames.PlayMaker.Tooltip("Power's value.")]
		public FsmInt fieldPower;
		
		[HutongGames.PlayMaker.Tooltip("Defense's value.")]
		public FsmInt fieldDefense;
		
		[HutongGames.PlayMaker.Tooltip("Range's value.")]
		public FsmInt fieldRange;
		
		[HutongGames.PlayMaker.Tooltip("Color's value.")]
		public FsmColor fieldColor;
		
		public override void OnEnter()
		{
			Card item = new Card();
			item.Name = fieldName.Value;
			item.LargeIcon = (Sprite) fieldLargeIcon.Value;
			item.SmallIcon = (Sprite) fieldSmallIcon.Value;
			item.MediumIcon = (Sprite) fieldMediumIcon.Value;
			item.Cost = fieldCost.Value;
			item.Power = fieldPower.Value;
			item.Defense = fieldDefense.Value;
			item.Range = fieldRange.Value;
			item.Color = fieldColor.Value;
			
			database.Add(item);
			Finish();
		}
	}
}
#endif
