using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardCount : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Card count.")]
		public FsmInt variable;

		public override void OnEnter()
		{
			variable.Value = database.Count();
			Finish();
		}
	}
}
#endif
