using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Card (Lightning Database)")]
	public class CardSave : FsmStateAction
	{
		[RequiredField]
		public CardDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Save slot index of Card")]
		public FsmInt saveSlot;

		public override void OnEnter()
		{
			database.Save(saveSlot.Value);
			Finish();
		}
	}
}
#endif
