using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconRemoveAll : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		public override void OnEnter()
		{
			database.Clear();
			Finish();
		}
	}
}
#endif
