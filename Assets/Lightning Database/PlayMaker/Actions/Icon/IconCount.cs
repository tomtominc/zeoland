using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconCount : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Icon count.")]
		public FsmInt variable;

		public override void OnEnter()
		{
			variable.Value = database.Count();
			Finish();
		}
	}
}
#endif
