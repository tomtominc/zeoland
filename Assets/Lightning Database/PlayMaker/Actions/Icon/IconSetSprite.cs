using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconSetSprite : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Icon's ID")]
		public FsmInt id;
		
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Icon's Sprite.")]
		public FsmObject value;

		public override void OnEnter()
		{
			Icon item = database.Find(id.Value);
			if (item != null)
			{
				item.Sprite = (Sprite) value.Value;
			}
			Finish();
		}
	}
}
#endif
