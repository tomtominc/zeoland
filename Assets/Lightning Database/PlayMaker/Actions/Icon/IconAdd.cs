using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconAdd : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;
		
		[HutongGames.PlayMaker.Tooltip("Name's value.")]
		public FsmString fieldName;
		
		[HutongGames.PlayMaker.Tooltip("Sprite's value.")]
		public FsmObject fieldSprite;
		
		public override void OnEnter()
		{
			Icon item = new Icon();
			item.Name = fieldName.Value;
			item.Sprite = (Sprite) fieldSprite.Value;
			
			database.Add(item);
			Finish();
		}
	}
}
#endif
