using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconSave : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Save slot index of Icon")]
		public FsmInt saveSlot;

		public override void OnEnter()
		{
			database.Save(saveSlot.Value);
			Finish();
		}
	}
}
#endif
