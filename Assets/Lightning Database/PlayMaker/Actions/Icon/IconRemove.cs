using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconRemove : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Icon's ID.")]
		public FsmInt id;

		public override void OnEnter()
		{
			database.Remove(id.Value);
			Finish();
		}
	}
}
#endif
