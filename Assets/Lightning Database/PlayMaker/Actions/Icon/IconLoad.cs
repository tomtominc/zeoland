using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconLoad : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Save slot index of Icon")]
		public FsmInt saveSlot;

		public override void OnEnter()
		{
			database.Load(saveSlot.Value);
			Finish();
		}
	}
}
#endif
