using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Icon (Lightning Database)")]
	public class IconGetName : FsmStateAction
	{
		[RequiredField]
		public IconDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Icon's ID")]
		public FsmInt id;
		
		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Icon's Name.")]
		public FsmString variable;

		public override void OnEnter()
		{
			Icon item = database.Find(id.Value);
			if (item != null)
			{
				variable.Value = item.Name;
			}
			Finish();
		}
	}
}
#endif
