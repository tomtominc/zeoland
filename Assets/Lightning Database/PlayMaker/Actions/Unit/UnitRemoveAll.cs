using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Unit (Lightning Database)")]
	public class UnitRemoveAll : FsmStateAction
	{
		[RequiredField]
		public UnitDatabase database;

		public override void OnEnter()
		{
			database.Clear();
			Finish();
		}
	}
}
#endif
