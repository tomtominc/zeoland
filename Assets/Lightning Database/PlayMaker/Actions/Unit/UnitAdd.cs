using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Unit (Lightning Database)")]
	public class UnitAdd : FsmStateAction
	{
		[RequiredField]
		public UnitDatabase database;
		
		[HutongGames.PlayMaker.Tooltip("Name's value.")]
		public FsmString fieldName;
		
		[HutongGames.PlayMaker.Tooltip("Color's value.")]
		public FsmColor fieldColor;
		
		[HutongGames.PlayMaker.Tooltip("BaseHealth's value.")]
		public FsmInt fieldBaseHealth;
		
		[HutongGames.PlayMaker.Tooltip("BasePower's value.")]
		public FsmInt fieldBasePower;
		
		[HutongGames.PlayMaker.Tooltip("BaseDefense's value.")]
		public FsmInt fieldBaseDefense;
		
		[HutongGames.PlayMaker.Tooltip("BaseMagic's value.")]
		public FsmInt fieldBaseMagic;
		
		[HutongGames.PlayMaker.Tooltip("BaseResistence's value.")]
		public FsmInt fieldBaseResistence;
		
		[HutongGames.PlayMaker.Tooltip("BaseSpeed's value.")]
		public FsmInt fieldBaseSpeed;
		
		[HutongGames.PlayMaker.Tooltip("BaseRange's value.")]
		public FsmInt fieldBaseRange;
		
		public override void OnEnter()
		{
			Unit item = new Unit();
			item.Name = fieldName.Value;
			item.Color = fieldColor.Value;
			item.BaseHealth = fieldBaseHealth.Value;
			item.BasePower = fieldBasePower.Value;
			item.BaseDefense = fieldBaseDefense.Value;
			item.BaseMagic = fieldBaseMagic.Value;
			item.BaseResistence = fieldBaseResistence.Value;
			item.BaseSpeed = fieldBaseSpeed.Value;
			item.BaseRange = fieldBaseRange.Value;
			
			database.Add(item);
			Finish();
		}
	}
}
#endif
