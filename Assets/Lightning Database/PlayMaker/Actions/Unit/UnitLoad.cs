using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Unit (Lightning Database)")]
	public class UnitLoad : FsmStateAction
	{
		[RequiredField]
		public UnitDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Save slot index of Unit")]
		public FsmInt saveSlot;

		public override void OnEnter()
		{
			database.Load(saveSlot.Value);
			Finish();
		}
	}
}
#endif
