using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Unit (Lightning Database)")]
	public class UnitCount : FsmStateAction
	{
		[RequiredField]
		public UnitDatabase database;

		[UIHint(UIHint.Variable)]
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Unit count.")]
		public FsmInt variable;

		public override void OnEnter()
		{
			variable.Value = database.Count();
			Finish();
		}
	}
}
#endif
