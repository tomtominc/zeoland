using UnityEngine;
using System.Collections;

#if PLAYMAKER
using HutongGames.PlayMaker;


namespace RutCreate.LightningDatabase.PlayMaker.Actions
{
	[ActionCategory("Unit (Lightning Database)")]
	public class UnitSetColor : FsmStateAction
	{
		[RequiredField]
		public UnitDatabase database;

		[RequiredField]
		[HutongGames.PlayMaker.Tooltip("Unit's ID")]
		public FsmInt id;
		
		[HutongGames.PlayMaker.Tooltip("Variable to assign value of Unit's Color.")]
		public FsmColor value;

		public override void OnEnter()
		{
			Unit item = database.Find(id.Value);
			if (item != null)
			{
				item.Color = value.Value;
			}
			Finish();
		}
	}
}
#endif
