using UnityEngine;
using System.Collections.Generic;
{{RequiredNamespaces}}

namespace RutCreate.LightningDatabase
{
	public abstract class Base{{ Class.Name }} : BaseClass
	{
		public Base{{Class.Name}}() {}

		public Base{{Class.Name}}(Base{{Class.Name}} source) : base(source)
		{
			{% for Field in Class.Fields %}{% if Field.Name != "ID" and Field.Name != "Name" %}{% if Field.is_list %}m_{{Field.Name}} = new {{Field.formatted_type}}(source.{{Field.Name}});{% else %}m_{{Field.Name}} = source.{{Field.Name}};{% endif %}{% endif %}
			{% endfor %}
		}

		public Base{{Class.Name}}(string serializedData) : base(serializedData)
		{
			{% for Field in Class.Fields %}{% if Field.Name != "ID" and Field.Name != "Name" and Field.is_serializable %}if (m_SerializedFields.ContainsKey("{{Field.Name}}"))
			{
				{% if Field.is_list %}{{Field.Name}} = RuntimeSerializer.ConvertToList{{Field.original_type}}(m_SerializedFields["{{Field.Name}}"]);{% else %}{{Field.Name}} = RuntimeSerializer.ConvertTo{{Field.original_type}}(m_SerializedFields["{{Field.Name}}"]);{% endif %}
			}
			{% endif %}{% endfor %}
		}

		{% for Field in Class.Fields %}{% if Field.Name != "ID" and Field.Name != "Name" %}{% if Field.is_class_field %}
		[SerializeField]
		[HideInInspector]
		protected {% if Field.is_list %}List<{{ Field.class_field_name }}>{% else %}{{ Field.class_field_name }}{% endif %} m_{{ Field.Name }} = {% if Field.is_list %}new List<{{ Field.class_field_name }}>(){% else %}null{% endif %};

		public virtual {% if Field.is_list %}List<{{ Field.class_field_name }}>{% else %}{{ Field.class_field_name }}{% endif %} {{ Field.Name }}
		{
			get { return m_{{ Field.Name }}; }
			set { m_{{ Field.Name }} = value; }
		}

		[SerializeField]
		protected {% if Field.is_list %}List<int>{% else %}int{% endif %} m_{{ Field.Name }}ID = {% if Field.is_list %}new List<int>(){% else %}-1{% endif %};

		public virtual {% if Field.is_list %}List<int>{% else %}int{% endif %} {{ Field.Name }}ID
		{
			get { return m_{{ Field.Name }}ID; }
			set { m_{{ Field.Name }}ID = value; }
		}
		{% else %}[SerializeField]
		protected {{ Field.formatted_type }} m_{{ Field.Name }} = {{ Field.default_value }};

		public virtual {{ Field.formatted_type }} {{ Field.Name }}
		{
			get { return m_{{ Field.Name }}; }
			set { m_{{ Field.Name }} = value; }
		}{% endif %}
		{% endif %}{% endfor %}

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			{% for Field in Class.Fields %}{%if Field.Name != "ID" and Field.Name != "Name" and Field.is_serializable %}
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("{{Field.Name}}");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.Serialize{{Field.original_type}}({{Field.Name}}));
			{% endif %}{% endfor %}
			return sb.ToString();
		}
	}
}
