﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("KeyValue", "Custom", typeof(KeyValue), "KeyValue", "new KeyValue()")]
	[System.Serializable]
	public class MyKeyValueField : FieldType
	{
		public override object DrawField(object item)
		{
			EditorUtil.SetLabelWidth(50f);
			EditorGUILayout.BeginVertical();
			KeyValue value = (item == null) ? new KeyValue() : (KeyValue)item;
			value.Key = EditorGUILayout.TextField("Key", value.Key, GUILayout.MinWidth(0f));
			value.Value = EditorGUILayout.TextField("Value", value.Value, GUILayout.MinWidth(0f));
			EditorGUILayout.EndVertical();
			EditorUtil.ResetLabelWidth();
			return value;
		}

		public override object GetDefaultValue()
		{
			return new KeyValue();
		}
	}
}