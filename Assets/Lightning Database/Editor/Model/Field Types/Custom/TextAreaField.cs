﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("String (TextArea)", "Custom", typeof(string), "string", "string.Empty")]
	[System.Serializable]
	public class TextAreaField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.TextArea((item == null) ? string.Empty : item.ToString(), GUILayout.MinWidth(0f));
			return item;
		}
	}
}
