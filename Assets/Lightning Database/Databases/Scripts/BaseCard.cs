using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCard : BaseClass
	{
		public BaseCard() {}

		public BaseCard(BaseCard source) : base(source)
		{
			
			
			m_Element = source.Element;
			m_CardType = source.CardType;
			m_CardEffectType = source.CardEffectType;
			m_LargeIcon = source.LargeIcon;
			m_SmallIcon = source.SmallIcon;
			m_MediumIcon = source.MediumIcon;
			m_Cost = source.Cost;
			m_Power = source.Power;
			m_Defense = source.Defense;
			m_Range = source.Range;
			m_Description = source.Description;
			m_Color = source.Color;
			
		}

		public BaseCard(string serializedData) : base(serializedData)
		{
			if (m_SerializedFields.ContainsKey("Cost"))
			{
				Cost = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Cost"]);
			}
			if (m_SerializedFields.ContainsKey("Power"))
			{
				Power = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Power"]);
			}
			if (m_SerializedFields.ContainsKey("Defense"))
			{
				Defense = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Defense"]);
			}
			if (m_SerializedFields.ContainsKey("Range"))
			{
				Range = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Range"]);
			}
			if (m_SerializedFields.ContainsKey("Description"))
			{
				Description = RuntimeSerializer.ConvertToString(m_SerializedFields["Description"]);
			}
			if (m_SerializedFields.ContainsKey("Color"))
			{
				Color = RuntimeSerializer.ConvertToColor(m_SerializedFields["Color"]);
			}
			
		}

		[SerializeField]
		protected Element m_Element = Element.Neutral;

		public virtual Element Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}
		[SerializeField]
		protected CardType m_CardType = CardType.Spell;

		public virtual CardType CardType
		{
			get { return m_CardType; }
			set { m_CardType = value; }
		}
		[SerializeField]
		protected CardEffectType m_CardEffectType = CardEffectType.Damage;

		public virtual CardEffectType CardEffectType
		{
			get { return m_CardEffectType; }
			set { m_CardEffectType = value; }
		}
		[SerializeField]
		protected Sprite m_LargeIcon = null;

		public virtual Sprite LargeIcon
		{
			get { return m_LargeIcon; }
			set { m_LargeIcon = value; }
		}
		[SerializeField]
		protected Sprite m_SmallIcon = null;

		public virtual Sprite SmallIcon
		{
			get { return m_SmallIcon; }
			set { m_SmallIcon = value; }
		}
		[SerializeField]
		protected Sprite m_MediumIcon = null;

		public virtual Sprite MediumIcon
		{
			get { return m_MediumIcon; }
			set { m_MediumIcon = value; }
		}
		[SerializeField]
		protected int m_Cost = 0;

		public virtual int Cost
		{
			get { return m_Cost; }
			set { m_Cost = value; }
		}
		[SerializeField]
		protected int m_Power = 0;

		public virtual int Power
		{
			get { return m_Power; }
			set { m_Power = value; }
		}
		[SerializeField]
		protected int m_Defense = 0;

		public virtual int Defense
		{
			get { return m_Defense; }
			set { m_Defense = value; }
		}
		[SerializeField]
		protected int m_Range = 0;

		public virtual int Range
		{
			get { return m_Range; }
			set { m_Range = value; }
		}
		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}
		[SerializeField]
		protected Color m_Color = new Color(1f, 1f, 1f, 1f);

		public virtual Color Color
		{
			get { return m_Color; }
			set { m_Color = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Cost");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Cost));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Power");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Power));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Defense");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Defense));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Range");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Range));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Description");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeString(Description));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Color");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeColor(Color));
			
			return sb.ToString();
		}
	}
}
