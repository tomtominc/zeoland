using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseEffectResolver : BaseClass
	{
		public BaseEffectResolver() {}

		public BaseEffectResolver(BaseEffectResolver source) : base(source)
		{
			
			
			m_Effect = source.Effect;
			
		}

		public BaseEffectResolver(string serializedData) : base(serializedData)
		{
			
		}

		[SerializeField]
		protected GameObject m_Effect = null;

		public virtual GameObject Effect
		{
			get { return m_Effect; }
			set { m_Effect = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			return sb.ToString();
		}
	}
}
