using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Card : BaseCard
	{
		public Card() {}

		public Card(Card source) : base(source) {}

		public Card(string serializedData) : base(serializedData) {}
	}
}
