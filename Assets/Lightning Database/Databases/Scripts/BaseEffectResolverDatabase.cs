using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseEffectResolverDatabase : BaseDatabase<EffectResolver>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			EffectResolver item = new EffectResolver(serializedData);
			m_Items.Add(item);
		}
	}
}
