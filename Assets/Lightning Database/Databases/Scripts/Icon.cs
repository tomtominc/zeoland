using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Icon : BaseIcon
	{
		public Icon() {}

		public Icon(Icon source) : base(source) {}

		public Icon(string serializedData) : base(serializedData) {}
	}
}
