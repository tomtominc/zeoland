using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseUnitDatabase : BaseDatabase<Unit>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			Unit item = new Unit(serializedData);
			m_Items.Add(item);
		}
	}
}
