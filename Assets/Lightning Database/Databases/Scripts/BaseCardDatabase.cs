using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCardDatabase : BaseDatabase<Card>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			Card item = new Card(serializedData);
			m_Items.Add(item);
		}
	}
}
