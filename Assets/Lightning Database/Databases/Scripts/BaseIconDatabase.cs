using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseIconDatabase : BaseDatabase<Icon>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			Icon item = new Icon(serializedData);
			m_Items.Add(item);
		}
	}
}
