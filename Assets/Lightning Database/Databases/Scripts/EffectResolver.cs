using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class EffectResolver : BaseEffectResolver
	{
		public EffectResolver() {}

		public EffectResolver(EffectResolver source) : base(source) {}

		public EffectResolver(string serializedData) : base(serializedData) {}
	}
}
