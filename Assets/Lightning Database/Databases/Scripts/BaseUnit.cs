using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseUnit : BaseClass
	{
		public BaseUnit() {}

		public BaseUnit(BaseUnit source) : base(source)
		{
			
			
			m_Team = source.Team;
			m_Element = source.Element;
			m_Color = source.Color;
			m_BaseHealth = source.BaseHealth;
			m_BasePower = source.BasePower;
			m_BaseDefense = source.BaseDefense;
			m_BaseMagic = source.BaseMagic;
			m_BaseResistence = source.BaseResistence;
			m_BaseSpeed = source.BaseSpeed;
			m_BaseRange = source.BaseRange;
			m_BoardAnimator = source.BoardAnimator;
			m_OverlayAnimator = source.OverlayAnimator;
			
		}

		public BaseUnit(string serializedData) : base(serializedData)
		{
			if (m_SerializedFields.ContainsKey("Color"))
			{
				Color = RuntimeSerializer.ConvertToColor(m_SerializedFields["Color"]);
			}
			if (m_SerializedFields.ContainsKey("BaseHealth"))
			{
				BaseHealth = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseHealth"]);
			}
			if (m_SerializedFields.ContainsKey("BasePower"))
			{
				BasePower = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BasePower"]);
			}
			if (m_SerializedFields.ContainsKey("BaseDefense"))
			{
				BaseDefense = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseDefense"]);
			}
			if (m_SerializedFields.ContainsKey("BaseMagic"))
			{
				BaseMagic = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseMagic"]);
			}
			if (m_SerializedFields.ContainsKey("BaseResistence"))
			{
				BaseResistence = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseResistence"]);
			}
			if (m_SerializedFields.ContainsKey("BaseSpeed"))
			{
				BaseSpeed = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseSpeed"]);
			}
			if (m_SerializedFields.ContainsKey("BaseRange"))
			{
				BaseRange = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BaseRange"]);
			}
			
		}

		[SerializeField]
		protected Team m_Team = Team.Character;

		public virtual Team Team
		{
			get { return m_Team; }
			set { m_Team = value; }
		}
		[SerializeField]
		protected Element m_Element = Element.Neutral;

		public virtual Element Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}
		[SerializeField]
		protected Color m_Color = new Color(1f, 1f, 1f, 1f);

		public virtual Color Color
		{
			get { return m_Color; }
			set { m_Color = value; }
		}
		[SerializeField]
		protected int m_BaseHealth = 0;

		public virtual int BaseHealth
		{
			get { return m_BaseHealth; }
			set { m_BaseHealth = value; }
		}
		[SerializeField]
		protected int m_BasePower = 0;

		public virtual int BasePower
		{
			get { return m_BasePower; }
			set { m_BasePower = value; }
		}
		[SerializeField]
		protected int m_BaseDefense = 0;

		public virtual int BaseDefense
		{
			get { return m_BaseDefense; }
			set { m_BaseDefense = value; }
		}
		[SerializeField]
		protected int m_BaseMagic = 0;

		public virtual int BaseMagic
		{
			get { return m_BaseMagic; }
			set { m_BaseMagic = value; }
		}
		[SerializeField]
		protected int m_BaseResistence = 0;

		public virtual int BaseResistence
		{
			get { return m_BaseResistence; }
			set { m_BaseResistence = value; }
		}
		[SerializeField]
		protected int m_BaseSpeed = 0;

		public virtual int BaseSpeed
		{
			get { return m_BaseSpeed; }
			set { m_BaseSpeed = value; }
		}
		[SerializeField]
		protected int m_BaseRange = 0;

		public virtual int BaseRange
		{
			get { return m_BaseRange; }
			set { m_BaseRange = value; }
		}
		[SerializeField]
		protected SpriteAnimationAsset m_BoardAnimator = null;

		public virtual SpriteAnimationAsset BoardAnimator
		{
			get { return m_BoardAnimator; }
			set { m_BoardAnimator = value; }
		}
		[SerializeField]
		protected SpriteAnimationAsset m_OverlayAnimator = null;

		public virtual SpriteAnimationAsset OverlayAnimator
		{
			get { return m_OverlayAnimator; }
			set { m_OverlayAnimator = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Color");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeColor(Color));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseHealth");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseHealth));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BasePower");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BasePower));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseDefense");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseDefense));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseMagic");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseMagic));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseResistence");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseResistence));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseSpeed");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseSpeed));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BaseRange");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BaseRange));
			
			return sb.ToString();
		}
	}
}
