using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Unit : BaseUnit
	{
		public Unit() {}

		public Unit(Unit source) : base(source) {}

		public Unit(string serializedData) : base(serializedData) {}
	}
}
