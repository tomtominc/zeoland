using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseIcon : BaseClass
	{
		public BaseIcon() {}

		public BaseIcon(BaseIcon source) : base(source)
		{
			
			
			m_Sprite = source.Sprite;
			
		}

		public BaseIcon(string serializedData) : base(serializedData)
		{
			
		}

		[SerializeField]
		protected Sprite m_Sprite = null;

		public virtual Sprite Sprite
		{
			get { return m_Sprite; }
			set { m_Sprite = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			return sb.ToString();
		}
	}
}
