﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;

public class TransitionCamera : MonoBehaviour
{
    private static TransitionCamera instance;

    public static TransitionCamera Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<TransitionCamera>();

            return instance;
        }
    }

    public enum EffectType
    {
        Fade,
        Distort
    }

    [System.SerializableAttribute]
    public class TransitionEffect
    {
        public string name;
        public int screenFlashCount = 0;
        public EffectType effectType;
        public Texture effectTexture;
        public Color effectColor;
    }

    public float transitionDuration = 1.0f;
    public float flashDuration = 0.1f;
    public Material transitionMaterial;
    public List<TransitionEffect> transitionEffects;
    public Dictionary<string, TransitionEffect> _transitions;

    public void Start()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);

        _transitions = new Dictionary<string, TransitionEffect>();

        foreach (var transition in transitionEffects)
            _transitions.Add(transition.name, transition);


        GetComponent<Camera>().enabled = false;
    }



    public void Transition(string transitionOutName, string transitionInName, string levelName)
    {
        transform.position = Camera.main.transform.position;
        GetComponent<Camera>().enabled = true;
        if (!_transitions.ContainsKey(transitionOutName))
        {
            Debug.LogWarningFormat("Transition Out does not contain {0} using default.", transitionOutName);
            transitionOutName = "Default";
        }

        StartCoroutine(TransitionRoutine(transitionOutName, transitionInName, levelName));

    }

    private IEnumerator TransitionRoutine(string transitionOutName, string transitionInName, string levelName)
    {
        TransitionEffect effect = _transitions[transitionOutName];

        for (int i = 0; i < effect.screenFlashCount; i++)
        {
            transitionMaterial.SetFloat("_Cutoff", 1f);
            transitionMaterial.SetColor("_Color", Color.white);
            transitionMaterial.SetFloat("_Distort", 0f);
            transitionMaterial.SetFloat("_Fade", 1f);

            yield return new WaitForSeconds(flashDuration);

            transitionMaterial.SetFloat("_Fade", 0f);

            yield return new WaitForSeconds(flashDuration);

        }

       
        transitionMaterial.SetColor("_Color", effect.effectColor);
       
        transitionMaterial.SetTexture("_TransitionTex", effect.effectTexture);
        transitionMaterial.SetFloat("_Distort", effect.effectType == EffectType.Fade ? 0f : 1f);


        if (effect.effectType == EffectType.Distort)
        {
            transitionMaterial.SetFloat("_Cutoff", 0f);
            transitionMaterial.SetFloat("_Fade", 1f);

            transitionMaterial.DOFloat(1f, "_Cutoff", transitionDuration).OnComplete
            (
                () =>
                {
                    StartCoroutine(LoadLevel(transitionInName, levelName));
                }
            );
        }
        else if (effect.effectType == EffectType.Fade)
        {
            transitionMaterial.SetFloat("_Cutoff", 1f);
            transitionMaterial.SetFloat("_Fade", 0f);

            transitionMaterial.DOFloat(1f, "_Fade", transitionDuration).OnComplete 
            (
            
                () =>
                {
                    StartCoroutine(LoadLevel(transitionInName, levelName));
                }
            );

        }
    }

    private IEnumerator LoadLevel(string transitionInName, string levelName)
    {
        yield return SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);

        if (!_transitions.ContainsKey(transitionInName))
        {
            Debug.LogWarningFormat("Transition In does not contain {0} using default.", transitionInName);
            transitionInName = "Default";
        }

        TransitionEffect effect = _transitions[transitionInName];

        transitionMaterial.SetTexture("_TransitionTex", effect.effectTexture);
        transitionMaterial.SetFloat("_Distort", effect.effectType == EffectType.Fade ? 0f : 1f);
        transitionMaterial.DOFloat(0f, "_Cutoff", transitionDuration).OnComplete
        (
            () =>
            {
                GetComponent<Camera>().enabled = false;
            }
        );
    }

}
