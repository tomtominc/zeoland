﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemList : MonoBehaviour
{
    public RectTransform itemPrefab;
    public RectTransform content;
    public RectTransform selector;
    public Vector2 selectorOffsetPosition;
    public ScrollRect scrollRect;
    public Text title;

    [HideInInspector]
    public int itemCount = 0;
    [HideInInspector]
    public int currentItem = -1;
    [HideInInspector]
    public List < Entity > entities;

    protected UnitState currentEntity;

    private float percent { get { return (float)currentItem / (float)itemCount; } }

    public virtual void SetCurrentEntity(UnitState entity)
    {
        currentEntity = entity;
    }

    public virtual void CreateList(List < Entity > entities)
    {
        this.entities = entities;

        Debug.LogFormat("Item count: {0}", entities.Count);
        content.DestroyChildren();

        foreach (var entity in this.entities)
        {
            RectTransform item = Instantiate < RectTransform >(itemPrefab);
            item.SetParent(content, false);
            item.GetComponent < ItemView >().Inititlize(entity); 
        }

        itemCount = this.entities.Count;
        SetCurrentItem(1);

    }

    public virtual void SetCurrentItem(int direction)
    {
        if (currentItem > -1)
        {
            SetItem(currentItem, false);
        }

        currentItem = currentItem.Move(direction, itemCount - 1);

        SetItem(currentItem, true);
    }

    public float CenterToItem(RectTransform obj)
    {
        float normalizePosition = percent * 2f;// (percent * 0.5f);// + (47f * 0.5f);
        return normalizePosition;
    }

    public virtual void SetItem(int index, bool isOn)
    {
        ItemView item = content.GetChild(index).GetComponent < ItemView >();
        item.IsOn = isOn;

        if (item.IsOn)
        {
            if (item.entity != null)
                title.text = item.entity.Name.ToUpper();
            
            scrollRect.horizontalNormalizedPosition = CenterToItem(item.GetComponent < RectTransform >());
            selector.SetParent(item.transform, false);
            selector.localPosition = selectorOffsetPosition;
        }
    }
}
