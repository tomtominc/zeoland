﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class CircularItemList : ItemList
{
    public WeaponStatsView weaponStatsView;
    public int maxButtonsInCircle = 6;
    public float circleSize = 50f;
    public int startRotIndex = 4;
    private int currentDirection = 0;
    //private List < ItemView > items;
    private Transform currentChild;

    private float rotateAmount
    {
        get { return 360f / maxButtonsInCircle; }
    }

    public override void CreateList(List<Entity> entities)
    {
        this.entities = entities;

        //items = new List < ItemView >();

        content.DestroyChildren();
        content.eulerAngles = Vector3.zero;

        itemCount = this.entities.Count;

        FillCircle();
       
        SetCurrentItem(1);
    }

    public void FillCircle()
    {
        for (int i = 0; i < maxButtonsInCircle; i++)
        {
            // fills the circle but does not set items with content yet.

            RectTransform itemTransform = Instantiate < RectTransform >(itemPrefab);
            itemTransform.SetParent(content, false);

            float theta = (2 * Mathf.PI / maxButtonsInCircle) * (i + startRotIndex);
            itemTransform.localPosition = new Vector3(Mathf.Sin(theta), Mathf.Cos(theta)) * circleSize;
        }

        currentChild = content.GetChild(content.childCount - 1);
        Transform firstChild = content.GetChild(0);

        ItemView item = firstChild.GetComponent < ItemView >();
        item.Inititlize(entities[0]);

        item.transform.Next().GetComponent < ItemView >().Inititlize(entities[1]);
        item.transform.Previous().GetComponent < ItemView >().Inititlize(entities[entities.Count - 1]);


        foreach (Transform child in content)
        {
            ItemView childView = child.GetComponent < ItemView >();

            //if (childView.entity != null && childView.entity.Id == currentEntity.GetAbility().Id)
            //child.GetComponent < ItemView >().IsEquipped = true;
        }

    }

    public override void SetCurrentItem(int direction)
    {
        currentDirection = direction;
        base.SetCurrentItem(currentDirection);
    }

    public override void SetItem(int index, bool isOn)
    {
        if (isOn)
        {
            currentChild = currentChild.ChildInSteps(currentDirection);

            Transform nextChild = currentChild.ChildInSteps(currentDirection);

            nextChild.GetComponent < ItemView >().Inititlize
            (entities[currentItem.Move(currentDirection, entities.Count - 1)]);

            content.DORotate(new Vector3(0f, 0f, content.eulerAngles.z +
                    (rotateAmount * currentDirection)), 0.2f).OnComplete(SetItem);
        }
        else
        {
            ItemView item = currentChild.GetComponent  < ItemView >();
            item.IsOn = isOn;
            item.IsEquipped = false;
        }
    }

    private void SetItem()
    {
        ItemView item = currentChild.GetComponent < ItemView >();

//        if (item.entity != null && item.entity.Id == currentEntity.GetAbility().Id)
//            item.IsEquipped = true;
//        else
//            item.IsEquipped = false;
//
//        weaponStatsView.SetStats(currentEntity, currentEntity.GetAbility(), item.entity);

        item.IsOn = true;
    }
}
