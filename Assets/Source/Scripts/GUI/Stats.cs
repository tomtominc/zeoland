﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{
    public Text nameLabel;
    public Text hpLabel;
    public LayeredBar hpBar;
    public Text weaponLabel;
    public Text powerLabel;
    public Text hitLabel;
    public Text criticalLabel;
    public Image element;
    public StatArrow arrow;

    public void SetStats(UnitState attacker, UnitState defender)
    {
        if (nameLabel)
            nameLabel.text = attacker.GetDisplayName();

        //if (weaponLabel)
        // weaponLabel.text = attacker.GetWeapon().Name.ToUpper();

        if (element)
            element.sprite = Database.IconDatabase.Find(attacker.GetUnit().Element.ToString()).Sprite;

        if (hpLabel)
            hpLabel.text = attacker.GetHealth().ToString();

        if (hpBar)
            hpBar.SetHealth(attacker.GetHealth());

        if (powerLabel)
            powerLabel.text = Formula.GetDamage(attacker, defender).ToString();

        if (criticalLabel)
            criticalLabel.text = attacker.GetCriticalChance().ToString();

        if (hitLabel)
            hitLabel.text = Formula.GetAccuracy(attacker, defender).ToString();

        if (arrow)
        {
            float effectiveness = Formula.TypeEffectiveness(attacker.GetUnit().Element, 
                                      defender.GetUnit().Element);

            if (effectiveness > 1f)
                arrow.DoArrowEffect(StatArrow.State.High);
            else if (effectiveness < 1f)
                arrow.DoArrowEffect(StatArrow.State.Low);
            else
                arrow.DoArrowEffect(StatArrow.State.None);
        }

      
    }
}
