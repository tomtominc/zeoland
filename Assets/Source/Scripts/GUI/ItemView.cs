﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemView : MonoBehaviour
{
    public Image itemImage;
    public GameObject equipTag;
    public Entity entity;

    private bool _isOn = false;
    private bool _isEquipped = false;

    public bool IsOn
    {
        get
        {
            return _isOn;
        }

        set
        {
            _isOn = value;
            itemImage.material = new Material(itemImage.material);
            itemImage.material.SetFloat("_Sat", _isOn ? 1 : 0);
        }
    }

    public bool IsEquipped
    {
        get { return _isEquipped; }
        set
        {
            _isEquipped = value;

            equipTag.SetActive(_isEquipped);
        }
    }

    public void Inititlize(Entity entity)
    {
        itemImage = GetComponentInChildren < Image >();
        this.entity = entity;
        itemImage.sprite = AssetManager.Instance.GetSprite(this.entity.Key);
        IsOn = false;
    }

    private void LateUpdate()
    {
        transform.rotation = Quaternion.identity;
    }

   
}
