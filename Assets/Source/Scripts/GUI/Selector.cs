﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Selector : MonoBehaviour
{
    public RectTransform spriteRect;

    public void Start()
    {
        spriteRect.DOShakePosition(0.5f, new Vector3(0f, 2f, 0f), 1, 0, true, false).SetLoops(-1, LoopType.Yoyo);
    }
}
