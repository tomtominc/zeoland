﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetStatsMenu : MonoBehaviour
{
    public Stats playerStats;
    public Stats enemyStats;

    public void SetStats(UnitState player, UnitState enemy)
    {
        playerStats.SetStats(player, enemy);
        enemyStats.SetStats(enemy, player);
    }
}
