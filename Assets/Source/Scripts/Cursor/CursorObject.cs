﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorObject : MonoBehaviour
{
    public Vector2 offset;

    private void Start()
    {
        Cursor.visible = false;
    }

    private void OnGUI()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);    
        transform.position = new Vector3(pos.x + offset.x, pos.y + offset.y, 0f);
    }
}
