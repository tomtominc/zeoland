﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    private static Player instance;

    public static Player Instance
    {
        get
        {
            if (instance == null)
                instance = new Player();

            return instance;
        }
    }

    public int megaBytes = 0;
    public PlayerHand hand;

    public Player()
    {
        megaBytes = 0;
        hand = new PlayerHand();
    }
}
