﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

public class CardTask
{
    public static IEnumerator ResolveCardTask(MonoBehaviour behaviour, ActivateCardCommand command)
    {

        CardState cardState = GameManager.Instance.GetCardState(command.gameId);

        switch (cardState.GetCard().CardType)
        {
            case CardType.Pet:
                yield return behaviour.StartCoroutine(HandlePetCard(behaviour, command));
                break;

            case CardType.Spell:
                yield return behaviour.StartCoroutine(HandleSpellCard(behaviour, command));
                break;

            case CardType.Weapon:

                break;
        }

        yield break;
    }

    private static IEnumerator HandlePetCard(MonoBehaviour behaviour, ActivateCardCommand command)
    {
        yield break;
    }

    private static IEnumerator HandleWeaponCard(ActivateCardCommand command)
    {
        yield break;
    }

    private static IEnumerator HandleSpellCard(MonoBehaviour behaviour, ActivateCardCommand command)
    {
        CardState cardState = GameManager.Instance.GetCardState(command.gameId);

        switch (cardState.GetCard().CardEffectType)
        {
            case CardEffectType.Damage:
                yield return behaviour.StartCoroutine(HandleDamageEffect(behaviour, command, cardState));
                break;
            case CardEffectType.Heal:
                yield return behaviour.StartCoroutine(HandleHealEffect(behaviour, command, cardState));
                break;
        }
    }

    #region SPELL CARD EFFECTS

    private static IEnumerator HandleDamageEffect(MonoBehaviour behaviour, ActivateCardCommand command, CardState cardState)
    {
        EffectResolver effectResolver = Database.EffectResolverDatabase.Find("DamageEffect");

        foreach (int unitId in command.affectedUnits)
        {
            EffectData effectData = new EffectData();
            effectData.cardId = command.gameId;
            effectData.unitId = unitId;
            GameObject effect = Object.Instantiate < GameObject >(effectResolver.Effect);
            BoardEffect boardEffect = effect.GetComponent < BoardEffect >();
            behaviour.StartCoroutine(boardEffect.DoEffect(effectData));
        }

        yield return new WaitForSeconds(1f);
    }

    private static IEnumerator HandleHealEffect(MonoBehaviour behaviour, ActivateCardCommand command, CardState cardState)
    {
        List < CellView > triggeredCells = BoardView.Instance.GetCellViews
            (command.boardPoint, cardState.GetRange(), RangeType.Normal).FilterByOccupied();

        foreach (CellView cell in triggeredCells)
        {
            int occupiedId = cell.cellState.occupiedId;
            UnitState healedUnit = GameManager.Instance.GetUnitState(occupiedId);
            healedUnit.AddHealthCounters(cardState.GetPower());
            command.affectedUnits.Add(healedUnit.GameId);
        }

        yield return new WaitForSeconds(1f);
    }

    #endregion
}
