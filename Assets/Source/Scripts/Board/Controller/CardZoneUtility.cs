﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardZoneUtility
{
    public static void CardEnterZone(CellView enteredCell, CardState cardState)
    {
        PlayerHandGUI.triggerCard = true;
        PlayerHandGUI.triggeredPoint = BoardView.Instance.GetPoint(enteredCell.position);

        switch (cardState.GetCard().CardType)
        {
            case CardType.Pet:
                HandlePetCard(enteredCell, cardState, cardState.GetCard().Color);
                break;
            case CardType.Spell:
                HandleSpellCard(enteredCell, cardState, cardState.GetCard().Color);
                break;
            case CardType.Weapon:
                break;
        }
    }

    public static void CardExitZone(CellView exitedCell, CardState cardState)
    {
        if (cardState.GetCard().CardEffectType == CardEffectType.Damage)
        {
            PlayerHandGUI.triggerCard = false;

            BoardView.Instance.HighlightCellsByRange 
            (
                BoardView.Instance.GetPoint(exitedCell.position), 
                cardState.GetRange(), 
                RangeType.Normal,
                exitedCell.normalColor
            );
        }
    }

    private static void HandlePetCard(CellView cellView, CardState cardState, Color highlightColor)
    {
        BoardView.Instance.HighlightCellsByRange 
        (
            BoardView.Instance.GetPoint(cellView.position), 
            1, 
            RangeType.Normal,
            highlightColor
        );
    }

    private static void HandleSpellCard(CellView cellView, CardState cardState, Color highlightColor)
    {
        switch (cardState.GetCard().CardEffectType)
        {
            case CardEffectType.Damage:
                HandleDamageHighlight(cellView, cardState, highlightColor);
                break;
        }
    }

    private static void HandleDamageHighlight(CellView cellView, CardState cardState, Color highlightColor)
    {
        BoardView.Instance.HighlightCellsByRange 
        (
            BoardView.Instance.GetPoint(cellView.position), 
            cardState.GetRange(), 
            RangeType.Normal,
            highlightColor
        );
    }
}
