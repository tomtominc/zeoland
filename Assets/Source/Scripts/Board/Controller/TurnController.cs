﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;

public class TurnController : SingletonBehaviour < TurnController >
{
    public float initializeDelay = 2.0f;

    public int startingHandSize = 3;
    public string announcmentName = "TurnAnnouncement";
    public float announcementOnScreenTime = 1f;
    public TurnView turnView;
    public Team currentTurn;
    public int turnCount;

    public IEnumerator Start()
    {
        // create the battle field using data that the overworld set
        // the overworld should have set who's on the field / who goes first / etc.

        yield return new WaitForSeconds(initializeDelay);

        StartCoroutine(StartTurn(currentTurn));

    }

    public IEnumerator StartTurn(Team team)
    {
        turnView.Set(team);
        UIManager.ShowUiElement(announcmentName);
        yield return new WaitForSeconds(announcementOnScreenTime);
        UIManager.HideUiElement(announcmentName);
        yield return new WaitForSeconds(0.5f);

        CommandQueue commandQueue = new CommandQueue();

        List < int > cards = new List < int >();

        for (int i = 0; i < startingHandSize; i++)
        {
            cards.Add(Player.Instance.hand.DrawCard());
        }

        commandQueue.DoCommand(CommandFactory.CreateDrawMultipleCommand(cards));

        ViewResolver.Instance.AddCommandQueue(commandQueue);

        if (team == Team.Character)
            BoardStateManager.Instance.ChangeState(BoardState.BoardUpdate);
    }

    public void EndTurn()
    {
        if (currentTurn == Team.Character)
            currentTurn = Team.Enemy;
        else
            currentTurn = Team.Character;

        StartCoroutine(StartTurn(currentTurn));
    }


}
