﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;

public static class CommandFactory
{
    public static SummonCommand CreateSummonCommand(UnitState entityState, FlatHexPoint boardPoint)
    {
        SummonCommand summonCommand = new SummonCommand();
        summonCommand.summonedId = entityState.GameId;
        summonCommand.boardPoint = boardPoint;
        return summonCommand;
    }

    public static MoveCommand CreateMoveCommand(UnitState entityState, FlatHexPoint boardPoint)
    {
        MoveCommand moveCommand = new MoveCommand();
        moveCommand.moverId = entityState.GameId;
        moveCommand.fromPoint = BoardView.Instance.GetCurrentPoint(entityState.GameId);
        moveCommand.toPoint = boardPoint;
        return moveCommand;
    }

    public static AttackCommand CreateAttackCommand(UnitState attacker, FlatHexPoint targetBoardPoint)
    {
        AttackCommand attackCommand = new AttackCommand();
        attackCommand.attackerId = attacker.GameId;
        attackCommand.targetBoardPoint = targetBoardPoint;
        return attackCommand;
    }

    public static DrawCommand CreateDrawCommand(int cardId)
    {
        DrawCommand drawCommand = new DrawCommand();
        drawCommand.cardId = cardId;
        return drawCommand;
    }

    public static DrawMultipleCommand CreateDrawMultipleCommand(List <int> cardIds)
    {
        DrawMultipleCommand drawCommand = new DrawMultipleCommand();
        drawCommand.cardIds = cardIds;
        return drawCommand;
    }

    public static ActivateCardCommand CreateActivateCardCommand(int cardGameId, FlatHexPoint targetBoardPoint)
    {
        ActivateCardCommand activateCardCommand = new ActivateCardCommand();
        activateCardCommand.gameId = cardGameId;
        activateCardCommand.boardPoint = targetBoardPoint;
        return activateCardCommand;
    }
}
