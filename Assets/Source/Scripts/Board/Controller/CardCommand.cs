﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using Gamelogic.Grids;

public class CardCommand
{
    public static void ActivateCard(ActivateCardCommand command)
    {
        CardState cardState = GameManager.Instance.GetCardState(command.gameId);

        switch (cardState.GetCard().CardType)
        {
            case CardType.Pet:
                HandlePetCard(command);
                break;

            case CardType.Spell:
                HandleSpellCard(command);
                break;

            case CardType.Weapon:

                break;
        }
    }

    private static void HandlePetCard(ActivateCardCommand command)
    {
        CommandQueue commandQueue = new CommandQueue();

        CardState cardState = GameManager.Instance.GetCardState(command.gameId);
        UnitState summonedCard = GameManager.Instance.CreateUnitState(cardState.GetCard().Name, Team.Character);
        commandQueue.DoCommand(summonedCard, command.boardPoint, CommandType.Summon);

        ViewResolver.Instance.AddCommandQueue(commandQueue);
    }

    private static void HandleWeaponCard(ActivateCardCommand command)
    {

    }

    private static void HandleSpellCard(ActivateCardCommand command)
    {
        CardState cardState = GameManager.Instance.GetCardState(command.gameId);

        switch (cardState.GetCard().CardEffectType)
        {
            case CardEffectType.Damage:
                HandleDamageEffect(command, cardState);
                break;
            case CardEffectType.Heal:
                HandleHealEffect(command, cardState);
                break;
        }
    }

    #region SPELL CARD EFFECTS

    private static void HandleDamageEffect(ActivateCardCommand command, CardState cardState)
    {
        List < CellView > triggeredCells = BoardView.Instance.GetCellViews
            (command.boardPoint, cardState.GetRange(), RangeType.Normal).FilterByOccupied();

        command.affectedUnits = new List < int >();

        foreach (CellView cell in triggeredCells)
        {
            int occupiedId = cell.cellState.occupiedId;
            UnitState damagedUnit = GameManager.Instance.GetUnitState(occupiedId);
            damagedUnit.AddHealthCounters(-cardState.GetPower());
            command.affectedUnits.Add(damagedUnit.GameId);
        }
    }

    private static void HandleHealEffect(ActivateCardCommand command, CardState cardState)
    {
        List < CellView > triggeredCells = BoardView.Instance.GetCellViews
            (command.boardPoint, cardState.GetRange(), RangeType.Normal).FilterByOccupied();

        foreach (CellView cell in triggeredCells)
        {
            int occupiedId = cell.cellState.occupiedId;
            UnitState healedUnit = GameManager.Instance.GetUnitState(occupiedId);
            healedUnit.AddHealthCounters(cardState.GetPower());
            command.affectedUnits.Add(healedUnit.GameId);
        }
    }

    #endregion
}
