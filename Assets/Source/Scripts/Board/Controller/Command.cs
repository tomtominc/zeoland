﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;

public enum CommandType
{
    Summon,
    Move,
    Attack,
    Draw,
    DrawMultiple,
    ActivateCard

}

public class Command
{
    public CommandType commandType;
}

public class SummonCommand : Command
{
    public int summonedId;
    public FlatHexPoint boardPoint;

    public SummonCommand()
    {
        commandType = CommandType.Summon;
    }
}

public class MoveCommand : Command
{
    public int moverId;
    public FlatHexPoint fromPoint;
    public FlatHexPoint toPoint;

    public MoveCommand()
    {
        commandType = CommandType.Move;
    }
}

public class AttackCommand : Command
{
    public int attackerId = -1;
    public int defenderId = -1;
    public FlatHexPoint targetBoardPoint;

    public AttackCommand()
    {
        commandType = CommandType.Attack;
    }
}

public class DrawCommand : Command
{
    public int cardId;
    public int addedCardGameId;

    public DrawCommand()
    {
        commandType = CommandType.Draw;
    }
}

public class DrawMultipleCommand : Command
{
    public List < int > cardIds;
    public List < int > addedCardGameIds;

    public DrawMultipleCommand()
    {
        commandType = CommandType.DrawMultiple;
        addedCardGameIds = new List<int>();
    }
}

public class ActivateCardCommand : Command
{
    public int gameId;
    public FlatHexPoint boardPoint;
    public List < int > affectedUnits;

    public ActivateCardCommand()
    {
        commandType = CommandType.ActivateCard;
    }
}
