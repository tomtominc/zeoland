﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;
using System.Linq;

public class CommandQueue
{
    public Queue < Command > commands;
    private IGrid < CellState , FlatHexPoint > boardState;

    public CommandQueue()
    {
        commands = new Queue < Command >();
        boardState = BoardView.Instance.boardState;
    }

    public void DoCommand(UnitState entityState, FlatHexPoint boardPoint, CommandType commandType)
    {
        switch (commandType)
        {
            case CommandType.Summon:
                commands.Enqueue(CommandFactory.CreateSummonCommand(entityState, boardPoint));
                break;
            case CommandType.Move:
                commands.Enqueue(CommandFactory.CreateMoveCommand(entityState, boardPoint));
                break;
            case CommandType.Attack:
                commands.Enqueue(CommandFactory.CreateAttackCommand(entityState, boardPoint));
                break;
        }
    }

    public void DoCommand(Command command)
    {
        commands.Enqueue(command);
    }

    public void Resolve()
    {
        for (int i = 0; i < commands.Count; i++)
        {
            Command command = commands.ElementAt(i);

            switch (command.commandType)
            {
                case CommandType.Summon:
                    SummonCommand summonCommand = (SummonCommand)command;
                    CellState cellToSummonOn = boardState[summonCommand.boardPoint];
                    cellToSummonOn.SetOccupied(summonCommand.summonedId);
                    break;
                case CommandType.Move:
                    MoveCommand moveCommand = (MoveCommand)command;
                    CellState fromCell = boardState[moveCommand.fromPoint];
                    fromCell.SetOccupied(-1);
                    //BoardView.Instance.SetNeighborsMoveReady(moveCommand.fromPoint, -1);
                    CellState toCell = boardState[moveCommand.toPoint];
                    toCell.SetOccupied(moveCommand.moverId);
                    break;
                case CommandType.Attack:
//                    AttackCommand attackCommand = (AttackCommand)command;
//                    CellState targetCell = boardState[attackCommand.targetBoardPoint];
//                    EntityState targetEntity = GameManager.Instance.GetEntityState(targetCell.occupiedId);
//                    EntityState attackerEntity = GameManager.Instance.GetEntityState(attackCommand.attackerId);
//                    int damageCounters = attackerEntity.GetPower();
//                    targetEntity.SetDamageCounters(damageCounters);
                    break;
                case CommandType.Draw:
                    DrawCommand drawCommand = (DrawCommand)command;
                    CardState cardState = GameManager.Instance.CreateCardState(drawCommand.cardId);
                    Player.Instance.hand.cards.Add(cardState);
                    drawCommand.addedCardGameId = cardState.GameId;
                    break;
                case CommandType.DrawMultiple:
                    DrawMultipleCommand drawMultipleCommand = (DrawMultipleCommand)command;
                    foreach (int id in drawMultipleCommand.cardIds)
                    {
                        CardState cs = GameManager.Instance.CreateCardState(id);
                        Player.Instance.hand.cards.Add(cs);
                        drawMultipleCommand.addedCardGameIds.Add(cs.GameId);
                    }
                    break;
                case CommandType.ActivateCard:
                    CardCommand.ActivateCard((ActivateCardCommand)command);
                    break;

            }
        }
    }


}
