﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity
{
    public int Id = -1;
    public string Name;
    public string Key;

    public UnitType CharacterType;
    public EntityType EntityType;
    public Element Element;

    public int Level = 1;
    public int HitPoints = 1;
    public int MagicPoints = 1;

    public AttackType attackType;
    public int Power = 1;
    public int Defense = 1;
    public int MagicPower = 1;

    public int Speed = 1;
    public int Range = 1;
    public int Accuracy = 100;
    public int Critical = 0;

    public RangeType RangeType;
    public UnitType equipFlag;
    public string Description;

}

public enum Team
{
    Character,
    Enemy,
    NPC
}

public enum EntityType
{
    Unit,
    Ability,
    Card
}

public enum RangeType
{
    Normal,
    HalfDiagonal,
    Line
}

public enum UnitType
{
    Zeo,
    Limo
}

public enum AttackType
{
    None,
    Physical,
    Magical
}

public enum Element
{
    Neutral,
    Pyro,
    Plant,
    Aqua,
    Meca,
    Psy,
    Wood,
    Virus,
    Mighty
}
