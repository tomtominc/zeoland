﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Formula
{
    public static float[,] ElementalDamageRatios;

    /// <summary>
    /// Formula from pokemon.
    /// bulbapedia.bulbagarden.net/wiki/Damage
    /// </summary>
    /// <returns>The damage.</returns>
    /// <param name="attacker">Attacker.</param>
    /// <param name="target">Target.</param>
    public static int GetDamage(UnitState attacker, UnitState target)
    {
        float level = (float)attacker.GetLevel();
        float power = (float)attacker.GetPower();
        float defense = (float)target.GetDefense();
        float weaponPower = 1f;//(float)attacker.GetWeapon().Power;
        float modifier = GetModifier(attacker, target);

        float damage = (((2f * level) / 5f) * (power / defense) * (weaponPower)) * modifier;

        return (int)damage;
    }

    public static float GetModifier(UnitState attacker, UnitState target)
    {
        // TODO change type to point to the attackers weapon not the attacker unit
        float stab = attacker.GetUnit().Element == attacker.GetUnit().Element ? 1.5f : 1f;
        float typeEffectiveness = TypeEffectiveness(attacker.GetUnit().Element, target.GetUnit().Element);

        return stab * typeEffectiveness;
    }

    public static float TypeEffectiveness(Element attack, Element defense)
    {
        if (ElementalDamageRatios == null)
            InitElementalDamageRatios();

        return ElementalDamageRatios[(int)attack, (int)defense];
    }

    public static float GetAccuracy(UnitState attacker, UnitState target)
    {
        return Mathf.Clamp(attacker.GetHitRate() - target.GetEvadeChance(), 0f, 100f);
    }

    public static float GetCriticalHitMultiplier(UnitState attacker)
    {
        float randomValue = Random.Range(0f, 101f);

        return randomValue < attacker.GetCriticalChance() ? 2f : 1f;
    }

    private static void InitElementalDamageRatios()
    {
        int elementCount = EnumExtensions.ToValues<Element >().Count;

        ElementalDamageRatios = new float[ elementCount, elementCount ];


        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Plant] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Pyro] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Aqua] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Meca] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Wood] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Virus] = 1.0f;
        ElementalDamageRatios[(int)Element.Neutral, (int)Element.Mighty] = 1.0f;

        ElementalDamageRatios[(int)Element.Plant, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Plant] = 0.5f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Pyro] = 0.5f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Aqua] = 2.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Meca] = 1.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Wood] = 2.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Virus] = 0.0f;
        ElementalDamageRatios[(int)Element.Plant, (int)Element.Mighty] = 1.0f;

        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Plant] = 2.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Pyro] = 0.5f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Aqua] = 0.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Meca] = 0.5f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Wood] = 2.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Virus] = 0.0f;
        ElementalDamageRatios[(int)Element.Pyro, (int)Element.Mighty] = 1.0f;

        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Plant] = 0.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Pyro] = 2.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Aqua] = 0.5f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Meca] = 2.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Wood] = 0.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Virus] = 1.0f;
        ElementalDamageRatios[(int)Element.Aqua, (int)Element.Mighty] = 1.0f;

        ElementalDamageRatios[(int)Element.Meca, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Plant] = 1.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Pyro] = 0.5f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Aqua] = 0.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Meca] = 1.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Psy] = 0.5f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Wood] = 2.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Virus] = 1.0f;
        ElementalDamageRatios[(int)Element.Meca, (int)Element.Mighty] = 1.0f;

        ElementalDamageRatios[(int)Element.Psy, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Plant] = 1.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Pyro] = 1.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Aqua] = 1.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Meca] = 2.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Psy] = 0.5f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Wood] = 1.0f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Virus] = 0.5f;
        ElementalDamageRatios[(int)Element.Psy, (int)Element.Mighty] = 2.0f;

        ElementalDamageRatios[(int)Element.Wood, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Plant] = 2.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Pyro] = 1.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Aqua] = 2.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Meca] = 0.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Wood] = 0.5f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Virus] = 0.0f;
        ElementalDamageRatios[(int)Element.Wood, (int)Element.Mighty] = 0.5f;

        ElementalDamageRatios[(int)Element.Virus, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Plant] = 1.0f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Pyro] = 0.5f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Aqua] = 2.0f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Meca] = 0.5f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Psy] = 2.0f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Wood] = 0.5f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Virus] = 0.5f;
        ElementalDamageRatios[(int)Element.Virus, (int)Element.Mighty] = 2.0f;

        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Neutral] = 1.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Plant] = 0.5f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Pyro] = 2.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Aqua] = 1.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Meca] = 1.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Psy] = 1.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Wood] = 0.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Virus] = 2.0f;
        ElementalDamageRatios[(int)Element.Mighty, (int)Element.Mighty] = 1.0f;

    }
}
