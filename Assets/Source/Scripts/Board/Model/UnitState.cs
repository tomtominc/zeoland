﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;
using System.Runtime.CompilerServices;

public enum CardType
{
    Spell,
    Weapon,
    Pet
}

public enum CardEffectType
{
    None,
    Damage,
    Heal,
    Modifier
}

public class CardState
{
    public int CardId = -1;
    public int GameId = -1;

    public CardState(int cardId, int gameId)
    {
        CardId = cardId;
        GameId = gameId;
    }

    public Card GetCard()
    {
        return Database.CardDatabase.Find(CardId);
    }

    public int GetHealth()
    {
        return 0;
    }

    public int GetCost()
    {
        return GetCard().Cost;
    }

    public int GetPower()
    {
        return GetCard().Power;
    }

    public int GetDefense()
    {
        return GetCard().Defense;
    }

    public int GetRange()
    {
        return GetCard().Range;
    }

}

public class UnitState
{
    public int EntityId = -1;
    public int GameId = -1;
    public Team Team;

    private int level = 1;
    private int healthCounters;
    private int powerCounters;
    private int defenseCounters;
    private int speedCounters;
    private int rangeCounters;

    public int attachedWeapon = -1;

    public UnitState()
    {
        
    }

    public UnitState(int entityId, int gameId, Team team)
    {
        EntityId = entityId;
        GameId = gameId;
        Team = team;
    }

    public void AttachCard(int id)
    {
        Card card = Database.CardDatabase.Find(id);

        switch (card.CardType)
        {
            case CardType.Weapon:
                attachedWeapon = id;
                break;
        }
    }

    public void AddHealthCounters(int counters)
    {
        healthCounters += counters;
    }

    public void AddPowerCounters(int counters)
    {
        powerCounters += counters;
    }

    public void AddDefenseCounters(int counters)
    {
        defenseCounters += counters;
    }

    public void AddSpeedCounters(int counters)
    {
        speedCounters += counters;
    }

    public void AddRangeCounters(int counters)
    {
        rangeCounters += counters;
    }

    public Unit GetUnit()
    {
        return GameManager.Instance.GetUnit(EntityId);
    }

    public int GetLevel()
    {
        return level;
    }

    public CardState GetWeapon()
    {
        return null;
    }

    public string GetDisplayName()
    {
        return GetUnit().Name.Split('_')[0].ToUpper();
    }

    public int GetDefense()
    {
        return GetUnit().BaseDefense + defenseCounters;
    }

    public int GetHealth()
    {
        return GetUnit().BaseHealth + healthCounters;
    }

    public int GetPower()
    {
        return GetUnit().BasePower + powerCounters;
    }

    public int GetSpeed()
    {
        return GetUnit().BaseSpeed + speedCounters;
    }

    public int GetRange()
    {
        return GetUnit().BaseRange + rangeCounters;
    }

    public float GetCriticalChance()
    {
        return 0f; //GetSpeed() + GetWeapon().Critical;
    }

    public float GetHitRate()
    {
        return 0f;// (float)(GetWeapon().Accuracy + (GetLevel() * 2));
    }

    public float GetEvadeChance()
    {
        return (float)(GetSpeed() * 2);
    }
}
