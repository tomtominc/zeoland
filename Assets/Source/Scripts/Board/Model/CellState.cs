﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellState
{
    public int occupiedId = -1;
    public int moveReadyId = -1;
    public int attackReadyId = -1;

    public bool IsOccupied()
    {
        return occupiedId > -1;    
    }

    public bool IsMoveReady()
    {
        return moveReadyId > -1;
    }


    public bool IsAttackReady()
    {
        return attackReadyId > -1;
    }

    public void SetOccupied(int occupiedId)
    {
        this.occupiedId = occupiedId;
    }
}
