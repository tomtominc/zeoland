﻿using System.Collections;
using UnityEngine;
using Gamelogic.Grids;

public class EntryPoint : MonoBehaviour
{

    // Use this for initialization
    IEnumerator Start()
    {
        while (!BoardView.Instance.initialized)
            yield return new WaitForEndOfFrame();

        CommandQueue commandQueue = new CommandQueue();

        UnitState mainZeo = GameManager.Instance.CreateUnitState("Zeo", Team.Character);
        commandQueue.DoCommand(mainZeo, new FlatHexPoint(4, 1), CommandType.Summon);

        UnitState slimeRed = GameManager.Instance.CreateUnitState("Slime_Red", Team.Enemy);
        commandQueue.DoCommand(slimeRed, new FlatHexPoint(2, 2), CommandType.Summon);

        UnitState slimeRed2 = GameManager.Instance.CreateUnitState("Slime_Blue", Team.Enemy);
        commandQueue.DoCommand(slimeRed2, new FlatHexPoint(3, 0), CommandType.Summon);


        ViewResolver.Instance.AddCommandQueue(commandQueue);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
