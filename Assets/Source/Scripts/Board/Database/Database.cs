﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

public class Database : SingletonBehaviour < Database >
{
    public UnitDatabase unitDatabase;
    public CardDatabase cardDatabase;
    public IconDatabase iconDatabase;
    public EffectResolverDatabase effectResolverDatabase;

    public static UnitDatabase UnitDatabase
    {
        get { return Instance.unitDatabase; }
    }

    public static CardDatabase CardDatabase
    {
        get { return Instance.cardDatabase; }
    }

    public static IconDatabase IconDatabase
    {
        get { return Instance.iconDatabase; }
    }

    public static EffectResolverDatabase EffectResolverDatabase
    {
        get { return Instance.effectResolverDatabase; }
    }
}
