﻿using UnityEditor;
using UnityEngine;
using Framework;


namespace RutCreate.LightningDatabase
{
    
    [FieldInfo("Team", "Board Properties", typeof(Team), "Team", "Team.Character")]
    public class TeamField : FieldType
    {
        public override object DrawField(object item)
        {
            Team value = (item == null) ? Team.Character : (Team)item;
            value = (Team)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return Team.Character;
        }
    }

    [FieldInfo("Element", "Board Properties", typeof(Element), "Element", "Element.Neutral")]
    public class ElementField : FieldType
    {
        public override object DrawField(object item)
        {
            Element value = (item == null) ? Element.Neutral : (Element)item;
            value = (Element)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return Element.Neutral;
        }
    }

    [FieldInfo("CardType", "Board Properties", typeof(CardType), "CardType", "CardType.Spell")]
    public class CardTypeField : FieldType
    {
        public override object DrawField(object item)
        {
            CardType value = (item == null) ? CardType.Spell : (CardType)item;
            value = (CardType)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return CardType.Spell;
        }
    }

    [FieldInfo("CardEffectType", "Board Properties", typeof(CardEffectType), "CardEffectType", "CardEffectType.Damage")]
    public class CardEffectTypeField : FieldType
    {
        public override object DrawField(object item)
        {
            CardEffectType value = (item == null) ? CardEffectType.Damage : (CardEffectType)item;
            value = (CardEffectType)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return CardEffectType.Damage;
        }
    }


    [FieldInfo("SpriteAnimationAsset", "Board Properties", typeof(SpriteAnimationAsset), "SpriteAnimationAsset", "null")]
    public class SpriteAnimationAssetField : FieldType
    {
        public override object DrawField(object item)
        {
            item = EditorGUILayout.ObjectField((item == null) ? null : (SpriteAnimationAsset)item, typeof(SpriteAnimationAsset), false);
            return item;
        }
    }

    [FieldInfo("Runtime Animator Controller", "Unity", typeof(RuntimeAnimatorController), "RuntimeAnimatorController", "null")]
    public class RuntimeAnimatorControllerField : FieldType
    {
        public override object DrawField(object item)
        {
            item = EditorGUILayout.ObjectField((item == null) ? null : (RuntimeAnimatorController)item, 
                typeof(RuntimeAnimatorController), false);
            return item;
        }
    }
}