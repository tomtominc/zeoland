﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using System;

public class AnimatedText : MonoBehaviour
{
    public float duration = 0.1f;
    public float delay = 0.05f;
    public float height = 1f;
    public Ease textEase = Ease.OutBounce;
    public Sprite positiveCharacter;
    public Sprite negativeCharacter;
    public List < SpriteRenderer > renderers;
    public List < Sprite > numbers;

    public Sequence AnimateIn(int value)
    {
        renderers[0].sprite = value >= 0 ? positiveCharacter : negativeCharacter;
        var numList = value.ToString().Select(c => (int)Char.GetNumericValue(c)).ToList();

        Sequence sequence = DOTween.Sequence();

        sequence.Join(renderers[0].transform.DOLocalMoveY(height, duration).SetEase(textEase).OnStart(() => gameObject.SetActive(true)));

        for (int i = 0; i < numList.Count; i++)
        {
            renderers[i + 1].sprite = numbers[numList[i]];
            sequence.Join(renderers[i + 1].transform.DOLocalMoveY(height, duration).SetEase(textEase).SetDelay(delay));
        }

        return sequence;
    }
}
