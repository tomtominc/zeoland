﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DamageEffect : BoardEffect
{
    public AnimatedText damageText;
    public SpriteAnimation superEffectAnimator;
    public SpriteAnimation elementAnimator;
    public SpriteAnimation hitAnimator;

    public override IEnumerator DoEffect(EffectData effectData)
    {
        CardState cardState = GameManager.Instance.GetCardState(effectData.cardId);
        UnitView unitView = GameManager.Instance.GetUnitView(effectData.unitId);

        transform.position = unitView.position;


        Sequence sequence = DOTween.Sequence();
        elementAnimator.Play(cardState.GetCard().Element);

        sequence.AppendInterval(elementAnimator.GetCurrentAnimationLength()); 
        sequence.AppendCallback(() => hitAnimator.Play("Effect"));
        sequence.Append(unitView.PlayDamageAnimation());
        sequence.Join(damageText.AnimateIn(cardState.GetPower()));

        yield return sequence.WaitForCompletion();

        Destroy(gameObject);

    }
}
