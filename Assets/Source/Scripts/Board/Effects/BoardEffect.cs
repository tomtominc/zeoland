﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;

public class BoardEffect : MonoBehaviour
{
    public virtual IEnumerator DoEffect(EffectData effectData)
    {
        yield return null;
    }
}

public class EffectData
{
    public int cardId;
    public int unitId;
}
