﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Gamelogic.Grids;

public class PlayerHandGUI : SingletonBehaviour < PlayerHandGUI >
{
    public CardInfoPanel cardInfoPanel;
    public RectTransform activeCardContainer;

    private PlayerHandContainer container;
    private RectTransform rectTransform;

    // BAD-----------
    // PUT THIS SOMEWHERE ELSE IT'S A TOTAL HACK!
    public static bool triggerCard = false;
    public static FlatHexPoint triggeredPoint;

    private void Awake()
    {
        container = GetComponentInChildren < PlayerHandContainer >();
        rectTransform = GetComponent < RectTransform >();
        rectTransform.anchoredPosition = Vector2.zero;

        HideCardInfo();
    }

    Tweener punch;

    public void ShowCardInfo(CardState cardState)
    {
        if (punch != null)
            punch.Complete();
        
        punch = cardInfoPanel.cardView.GetComponent<RectTransform>().DOPunchScale(new Vector3(0f, 0.1f, 0f), 0.5f).
            SetEase(Ease.OutBack);
        cardInfoPanel.Refresh(cardState);
        cardInfoPanel.gameObject.SetActive(true);
    }

    public void HideCardInfo()
    {
        cardInfoPanel.gameObject.SetActive(false);
    }

    public IEnumerator AddCards(List < CardState > cardStates)
    {
        bool finished = false;

        container.AddCards(cardStates, () => finished = true);

        while (!finished)
            yield return null;

    }

    public void AnimateIn()
    {
        rectTransform.DOAnchorPosY(0f, 0.5f, true).SetEase(Ease.OutBack);
    }

    public void AnimateOut()
    {
        rectTransform.DOAnchorPosY(-30f, 0.5f, true).SetEase(Ease.InBack);
    }

   
}
