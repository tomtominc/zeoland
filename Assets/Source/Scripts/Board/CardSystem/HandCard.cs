﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
using RutCreate.LightningDatabase;
using DoozyUI;

public class HandCard : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, 
                                        IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public enum State
    {
        None,
        ShowingInfo,
        Dragging,
        Inactive
    }

    public float flipDuration = 0.4f;
    public float inDuration = 0.2f;
    public CanvasGroup canvasGroup;
    public LayoutElement layoutElement;
    public Vector2 perferredSize;
    public RectTransform container;
    public RectTransform cardFront;
    public RectTransform cardBack;
    public RectTransform cardActive;
    public RectTransform morphOverlay;

    public Outline outline;

    private State currentState;
    private CardView cardView;
    private RectTransform canvas;
    private Material hsvMaterial;

    private void Awake()
    {
        currentState = State.None;
        cardView = GetComponent < CardView >();
        canvas = UIManager.GetUiContainer.GetComponent < RectTransform >();
    }

    private void Start()
    {
        hsvMaterial = cardFront.GetComponent < Image >().material;
    }

    private void Update()
    {
        int saturation = cardView.cardState.GetCost() > Player.Instance.megaBytes ? 0 : 1;
        //cardFront.GetComponent < Image >().material.SetFloat("_Sat", saturation);
    }

    public Sequence AnimateIn()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(container.DOAnchorPosY(-25f, inDuration, true).From());
        sequence.Join(layoutElement.DOPreferredSize(perferredSize, inDuration));

        return sequence; 
    }

    public Sequence Flip()
    {
        outline.gameObject.SetActive(false);
        Sequence sequence = DOTween.Sequence();
        sequence.Append(cardBack.DOSizeDelta(new Vector2(0f, cardBack.sizeDelta.y), 
                flipDuration * 0.5f, true).SetEase(Ease.InBack));
        sequence.Append(cardFront.DOSizeDelta(new Vector2(0f, cardFront.sizeDelta.y), 
                flipDuration * 0.5f, true).SetEase(Ease.OutBack).From());
        sequence.AppendCallback(() => outline.gameObject.SetActive(true));
        return sequence;
    }

    public void SetInteractable(bool interactable)
    {
        canvasGroup.interactable = interactable;
        canvasGroup.blocksRaycasts = interactable;
    }

    bool allowForEndDragSequence = true;
    bool allowForBeginDragSequence = true;
    Sequence beginDragSequence;
    Sequence endDragSequence;

    public void OnBeginDrag(PointerEventData e)
    {
        if (currentState == State.Inactive)
            return;

        BoardStateManager.SetCursorGrab();
        currentState = State.Dragging;
        PlayerHandGUI.Instance.HideCardInfo();
        container.SetParent(PlayerHandGUI.Instance.activeCardContainer, true);
        Player.Instance.hand.SetCurrentCardId(cardView.cardState.GameId);
        BoardStateManager.Instance.ChangeState(BoardState.CardUpdate);


        StartBeginDragSequence();
    }

    private void StartBeginDragSequence()
    {
        if (endDragSequence != null && endDragSequence.IsActive())
            endDragSequence.Complete(true);

        beginDragSequence = DOTween.Sequence();

        beginDragSequence.AppendCallback(() =>
            {
                outline.gameObject.SetActive(false);
                morphOverlay.gameObject.SetActive(true);
                cardFront.gameObject.SetActive(false);
                morphOverlay.GetComponent < Image >().color = Color.white;
            });

        beginDragSequence.Append(morphOverlay.DOSizeDelta(new Vector2(15f, 18f), 0.2f));

        beginDragSequence.AppendCallback(() =>
            {
                cardActive.gameObject.SetActive(true);
                morphOverlay.GetComponent<Image>().color = Color.white;
            });

        beginDragSequence.Append(morphOverlay.GetComponent < Image >().DOFade(0f, 0.2f));

        beginDragSequence.AppendCallback(() =>
            {
                allowForEndDragSequence = true;
            });
    }

    public void OnDrag(PointerEventData e)
    {
        if (currentState == State.Inactive)
            return;
        
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 size = canvas.sizeDelta * 0.5f;
        container.position = mousePosition;

    }

    public void OnEndDrag(PointerEventData e)
    {
        if (currentState == State.Inactive)
            return;

        BoardStateManager.SetCursorNormal();

        currentState = State.Inactive;
        Player.Instance.hand.SetCurrentCardId(-1);

        if (PlayerHandGUI.triggerCard)
        {
            BoardStateManager.Instance.ChangeState(BoardState.WaitUpdate);

            morphOverlay.gameObject.SetActive(true);
            morphOverlay.GetComponent < Image >().color = Color.white;
            cardActive.gameObject.SetActive(false);

            morphOverlay.GetComponent < Image >().DOFade(0f, 0.5f).OnComplete(() =>
                {
                    CommandQueue commandQueue = new CommandQueue();
                    commandQueue.DoCommand(
                        CommandFactory.CreateActivateCardCommand
                        (cardView.cardState.GameId, PlayerHandGUI.triggeredPoint));
                    ViewResolver.Instance.AddCommandQueue(commandQueue, 
                        () => BoardStateManager.Instance.ChangeState(BoardState.BoardUpdate));

                    Destroy(gameObject);
                });
        }
        else
        {
            container.SetParent(transform, true);
            outline.enabled = false;
            container.DOAnchorPos(Vector2.zero, 0.2f, true).OnComplete(() => currentState = State.None);
            BoardStateManager.Instance.ChangeState(BoardState.BoardUpdate);
            StartEndDragSequence();
        }
       
       
    }

    private void StartEndDragSequence()
    {
        if (beginDragSequence != null && beginDragSequence.IsActive())
            beginDragSequence.Complete(true);
        
        endDragSequence = DOTween.Sequence();

        endDragSequence.AppendCallback(() =>
            {
                morphOverlay.gameObject.SetActive(true);
                morphOverlay.GetComponent < Image >().color = Color.white;
                cardActive.gameObject.SetActive(false);
            });

        endDragSequence.Append(morphOverlay.DOSizeDelta(new Vector2(20f, 27f), 0.2f));

        endDragSequence.AppendCallback(() =>
            {
                outline.gameObject.SetActive(true);
                cardFront.gameObject.SetActive(true);
            });

        endDragSequence.Append(morphOverlay.GetComponent < Image >().DOFade(0f, 0.2f));

        endDragSequence.AppendCallback(() =>
            {
                allowForEndDragSequence = true;
            });
    }

    public void OnPointerEnter(PointerEventData e)
    {
        if (currentState != State.None)
            return;

        BoardStateManager.SetCursorClickable();

        outline.enabled = true;
        container.DOAnchorPosY(4f, 0.1f, true);
        if (cardView.cardState != null)
            PlayerHandGUI.Instance.ShowCardInfo(cardView.cardState);
    }

    public void OnPointerExit(PointerEventData e)
    {
        if (currentState != State.None)
            return;

        BoardStateManager.SetCursorNormal();

        outline.enabled = false;
        container.DOAnchorPosY(0f, 0.1f, true);
        PlayerHandGUI.Instance.HideCardInfo();
    }
}
