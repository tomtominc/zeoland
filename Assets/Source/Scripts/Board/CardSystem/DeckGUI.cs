﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DeckGUI : MonoBehaviour
{
    public float deckMoveDuration = 0.5f;
    public float drawDuration = 0.2f;
    public RectTransform deck;
    public RectTransform card;
    public Vector2 cardMovePosition = new Vector2(-28f, -107f);


    public Tweener AnimateDeckIn()
    {
        deck.anchoredPosition = new Vector2(120f, 0f);
        return deck.DOAnchorPosX(-48f, deckMoveDuration, true).OnStart(() => deck.gameObject.SetActive(true));
    }

    public Tweener AnimateDeckOut()
    {
        return deck.DOAnchorPosX(120f, deckMoveDuration, true).OnComplete(() => deck.gameObject.SetActive(false));
    }

    public Tweener AnimateDrawCard()
    {
        return card.DOAnchorPos(cardMovePosition, drawDuration, true).OnComplete(() => card.anchoredPosition = Vector2.zero);
    }
}
