﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHand
{
    public List < CardState > cards;
    private Deck deck;

    private int currentCardId = -1;

    public PlayerHand()
    {
        cards = new List < CardState >();
        deck = new Deck();
    }

    public int DrawCard()
    {
        return deck.DrawCard();
    }

    public void SetCurrentCardId(int cardId)
    {
        currentCardId = cardId;
    }

    public int GetCurrentCardId()
    {
        return currentCardId;
    }
}
