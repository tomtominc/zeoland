﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInfoPanel : MonoBehaviour
{
    public CardView cardView;
    public CardPreview cardPreview;

    public void Refresh(CardState cardState)
    {
        cardView.RefreshView(cardState);
        cardPreview.Refresh(cardState.GetCard());
    }
}
