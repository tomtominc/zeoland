﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Deck
{
    public Dictionary < int , int > cards;

    public Deck()
    {
        cards = new Dictionary < int, int >();

        cards.Add(Database.CardDatabase.Find("Mata").ID, 1);
        cards.Add(Database.CardDatabase.Find("Fogo").ID, 1);
        //cards.Add(Database.CardDatabase.Find("H20").ID, 1);
        cards.Add(Database.CardDatabase.Find("Makina").ID, 1);
    }

    public int DrawCard()
    {
        if (cards.Count <= 0)
            return -1;
        
        var cardList = cards.ToList();

        int cardId = cardList[Random.Range(0, cardList.Count)].Key;

        cards[cardId]--;

        if (cards[cardId] <= 0)
            cards.Remove(cardId);

        return cardId;
    }
}
