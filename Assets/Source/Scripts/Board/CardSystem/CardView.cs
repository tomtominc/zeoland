﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RutCreate.LightningDatabase;

public class CardView : MonoBehaviour
{

    public Text nameLabel;
    public Text descriptionLabel;
    public Image cardFront;
    public Image portrait;
    public UISpriteText costLabel;
    public UISpriteText powerLabel;
    public UISpriteText healthLabel;


    public Image mediumIcon;
    public Image smallIcon;

    public CardState cardState;

    public void RefreshView(CardState cardState)
    {
        this.cardState = cardState;

        if (mediumIcon)
            mediumIcon.sprite = cardState.GetCard().MediumIcon;
        
        if (smallIcon)
            smallIcon.sprite = cardState.GetCard().SmallIcon;

        if (cardFront)
            cardFront.sprite = Database.IconDatabase.Find(string.Format("{0}Card", cardState.GetCard().Element)).Sprite;

        if (portrait)
            portrait.sprite = cardState.GetCard().LargeIcon;

        if (costLabel)
            costLabel.Text = cardState.GetCost().ToString();

        if (powerLabel)
            powerLabel.Text = cardState.GetPower().ToString();

        if (healthLabel)
            healthLabel.Text = cardState.GetHealth().ToString();

        if (nameLabel)
            nameLabel.text = cardState.GetCard().Name.ToUpper();
        
    }
}
