﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids.Examples;
using DG.Tweening;
using System;

public class PlayerHandContainer : MonoBehaviour
{
    public RectTransform handCardPrefab;
    public DeckGUI deckGUI;

    private List < HandCard > handCards;

    private void Awake()
    {
        handCards = new List < HandCard >();

        transform.DestroyChildren();
    }

    public void AddCard(CardState cardState)
    {
        
    }

    public void AddCards(List < CardState > cardStates, Action finished)
    {
        List < HandCard > newCards = new List < HandCard >();

        foreach (CardState cardState in cardStates)
        {
            RectTransform card = Instantiate < RectTransform >(handCardPrefab);
            card.gameObject.SetActive(false);
            card.SetParent(transform, false);
            CardView cardView = card.GetComponent < CardView >();
            cardView.RefreshView(cardState);
            HandCard handCard = card.GetComponent < HandCard >();
            handCard.SetInteractable(false);
            newCards.Add(card.GetComponent < HandCard >());
        }

        Sequence sequence = DOTween.Sequence();

        sequence.Append(deckGUI.AnimateDeckIn());

        for (int i = 0; i < newCards.Count; i++)
        {
            sequence.Append(deckGUI.AnimateDrawCard());
        }

        foreach (HandCard card in newCards)
        {
            sequence.AppendCallback(() => card.gameObject.SetActive(true));
            sequence.Append(card.AnimateIn());
        }

        foreach (HandCard card in newCards)
        {
            sequence.Append(card.Flip());
            sequence.AppendCallback(() => card.SetInteractable(true));
        }

        sequence.Append(deckGUI.AnimateDeckOut());
        sequence.OnComplete(() => finished());
    }
}
