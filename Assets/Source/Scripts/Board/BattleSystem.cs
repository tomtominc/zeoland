﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour
{
    private static BattleSystem instance;

    public static BattleSystem Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < BattleSystem >();
            return instance;
        }
    }

    private void Awake()
    {
        // Cursor.visible = false;
    }
}
