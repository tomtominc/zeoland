﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponStatsView : MonoBehaviour
{
    public Text nameLabel;
    public Image element;
    public StatText powerLabel;
    public StatText criticalLabel;
    public StatText accuracyLabel;
    public StatText avoidLabel;

    public void SetStats(UnitState character, Entity equippedAbility, Entity selectedAbility)
    {
        nameLabel.text = selectedAbility.Name.ToUpper();
        element.sprite = AssetManager.Instance.GetSprite(selectedAbility.Element);
        powerLabel.SetStat(equippedAbility.Power, selectedAbility.Power);
        criticalLabel.SetStat(equippedAbility.Critical, selectedAbility.Critical);
        accuracyLabel.SetStat(equippedAbility.Accuracy, selectedAbility.Accuracy);
        avoidLabel.SetStat(0, 0);
    }
   
}
