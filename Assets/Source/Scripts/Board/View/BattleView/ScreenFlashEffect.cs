﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenFlashEffect : Effect
{
    public SpriteRenderer flashImage;
    public float duration = 0.1f;
    public float turnOffDuration = 0.2f;

    public override IEnumerator DoEffect(Action onComplete)
    {
        Tweener flashTween = flashImage.DOFade(1f, duration);

        yield return flashTween.WaitForCompletion();

        yield return new WaitForSeconds(turnOffDuration);

        flashImage.DOFade(0f, turnOffDuration);

        if (onComplete != null)
            onComplete();

    }
}

public static class TextMeshProExtensions
{
    public static string Size(this string text, float percent)
    {
        return string.Format("<size={0}%>{1}</size>", percent, text);
    }

    public static string Sup(this string text)
    {
        return string.Format("<sup>{0}</sup>", text);
    }
}