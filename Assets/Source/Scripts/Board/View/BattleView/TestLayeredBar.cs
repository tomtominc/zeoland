﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLayeredBar : MonoBehaviour
{
    public LayeredBar bar;
    public int health;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            bar.SetHealth(health);     
        }
    }
}
