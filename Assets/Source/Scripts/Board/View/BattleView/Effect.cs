﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Effect : MonoBehaviour
{
    public abstract IEnumerator DoEffect(Action onComplete);
}
