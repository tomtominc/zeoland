﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;
using DoozyUI;
using System;

public class BattleView : MonoBehaviour
{
    private static BattleView instance;

    public static BattleView Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < BattleView >();

            return instance;
        }
    }

    public enum StartEffect
    {
        None,
        Flash
    }

    [Header("INSTANCE PROPERTIES")] 
    public ParallaxController parallaxController;
    public Transform characterLayer;
    public ScreenFlashEffect screenFlashEffect;

    [Header("PARALLAX PROPERTIES")]
    public Vector2 speed = new Vector2(1f, 0f);
    public Vector2 maxSpeed = new Vector2(10f, 0f);
    public float moveDuration = 5.0f;
    public Vector2 battleCenter = new Vector2(-13f, 0f);
    public Ease parallaxMoveSpeedEase;

    [Header("ENTITY BATTLE VIEWS")]
    public BattleUI battleUI;
    public EntityBattleView attacker;
    public EntityBattleView defender;

    private Vector3 originalParallaxPosition;
    private Vector2 updatedPosition;

    // events

    public EventHandler OnBattleEnd;

    public IEnumerator InitializeBattle(AttackCommand attackCommand)
    {
        UnitState attackerState = GameManager.Instance.GetUnitState(attackCommand.attackerId);
        UnitState defenderState = GameManager.Instance.GetUnitState(attackCommand.defenderId);

        attacker.Initialize(attackerState);
        defender.Initialize(defenderState);

        UpdateUI();

        yield return StartCoroutine(screenFlashEffect.DoEffect(Parallax));


    }

    public void Update()
    {
        UpdateUI();
    }

    // put somewhere else. this should probably go into a data class
    public void Damage(UnitState attacker, UnitState defender)
    {
        defender.AddHealthCounters(-Formula.GetDamage(attacker, defender));

        UpdateUI();
    }

    public void UpdateUI()
    {
        if (attacker.unitState != null && defender.unitState != null)
            battleUI.Initialize(attacker.unitState, defender.unitState);
    }

    public void EndBattle()
    {
        StartCoroutine(screenFlashEffect.DoEffect(() =>
                {
                    parallaxController.gameObject.SetActive(false);

                    if (OnBattleEnd != null)
                        OnBattleEnd(this, null);
                }));
    }

    public void Parallax()
    {
        parallaxController.gameObject.SetActive(true);
        attacker.StartIdle();
        defender.StartIdle();

        bool isDone = false;
        DOTween.To(() => speed, x => speed = x, maxSpeed, moveDuration)
            .SetEase(parallaxMoveSpeedEase)
            .OnUpdate 
            (
            () =>
            {
                if (characterLayer.position.x > battleCenter.x)
                    parallaxController.Move(speed, new Vector2(-1f, 0f));
                else if (!isDone)
                {
                    isDone = true;
                    battleCenter.y = characterLayer.position.y;
                    characterLayer.position = battleCenter;
                    StartCoroutine(Battle());
                }
            }
        );
    }

    public IEnumerator Battle()
    {
        UIManager.ShowUiElement("BattleUI");

        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(attacker.Attack(defender));
        yield return StartCoroutine(defender.Attack(attacker));

        UIManager.HideUiElement("BattleUI");
        EndBattle();
    }
}
