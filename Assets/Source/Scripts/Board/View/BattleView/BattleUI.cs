﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUI : MonoBehaviour
{
    public Stats attackerStats;
    public Stats defenderStats;

    public void Initialize(UnitState attacker, UnitState defender)
    {
        attackerStats.SetStats(attacker, defender);
        defenderStats.SetStats(defender, attacker);
    }
}
