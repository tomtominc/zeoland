﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LayeredBar : MonoBehaviour
{
    public bool snapping = true;
    public float pieceFillDuration = 0.01f;
    public float pieceWidth = 3.05f;
    public float maxBarWidth = 61.0f;
    public List < RectTransform > barLayers;

    private float currentBarLength = 0f;

    public void SetHealth(int healthValue)
    {
        float totalBarLength = pieceWidth * healthValue;
        float duration = (float)Mathf.Abs(totalBarLength - currentBarLength) * pieceFillDuration;

        DOTween.To(() => currentBarLength, x => currentBarLength = x, totalBarLength, duration).OnUpdate
        (
            () =>
            {
                SetBarLength(currentBarLength);
            }
        );
    }

    private void SetBarLength(float totalBarLength)
    {
        float percent = totalBarLength / maxBarWidth;
        int filledBars = (int)percent;
        int currentBar = Mathf.RoundToInt(percent + 0.5f) - 1;
        float sizeX = totalBarLength - (maxBarWidth * filledBars);
        float sizeY = barLayers[currentBar].sizeDelta.y;

        if (snapping)
        {
            sizeX = Mathf.Clamp(Mathf.Round((Mathf.Round(sizeX / pieceWidth) * pieceWidth) + 0.5f), 0f, maxBarWidth);
        }

        barLayers[currentBar].sizeDelta = new Vector2(sizeX, sizeY);
    }


}
