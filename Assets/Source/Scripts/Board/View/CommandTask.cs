﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using RutCreate.LightningDatabase;

public class CommandTask
{
    protected ViewResolver _context;
    protected Command _command;

    public CommandTask()
    {
        
    }

    public virtual void Initialize(ViewResolver context, Command command)
    {
        _context = context;
        _command = command;
    }

    public virtual IEnumerator Resolve()
    {
        yield break;
    }
}

public class SummonCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        SummonCommand summonCommand = (SummonCommand)_command;
        CellView cellView = BoardView.Instance.GetCell(summonCommand.boardPoint);

        UnitView unitView = PrefabManager.GetInstance(summonCommand.summonedId);
        unitView.transform.position = cellView.transform.position;

        unitView.SetPower(unitView.unitState.GetPower());
        unitView.SetHealth(unitView.unitState.GetHealth());

        cellView.Highlight(GameManager.Instance.GetUnitState(summonCommand.summonedId).GetUnit().Color);
        yield break;
    }
}

public class MoveCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        MoveCommand moveCommand = (MoveCommand)_command;

        UnitView entityView = GameManager.Instance.GetUnitView(moveCommand.moverId);

        yield return _context.StartCoroutine(entityView.Move(moveCommand));
    }
}

public class AttackCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        AttackCommand attackCommand = (AttackCommand)_command;
        attackCommand.defenderId = BoardView.Instance.GetCell
            (attackCommand.targetBoardPoint)
            .cellState.occupiedId;

        yield return _context.StartCoroutine(BattleView.Instance.InitializeBattle(attackCommand));
    }
}

public class DrawCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        yield return null;
    }
}

public class DrawMultipleCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        DrawMultipleCommand drawMultipleCommand = (DrawMultipleCommand)_command;
        List < CardState > cardStates = new List < CardState >();

        foreach (int id in drawMultipleCommand.addedCardGameIds)
        {
            cardStates.Add(GameManager.Instance.GetCardState(id));
        }


        yield return _context.StartCoroutine(PlayerHandGUI.Instance.AddCards(cardStates));

    }
}

public class ActivateCardCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        yield return _context.StartCoroutine(CardTask.ResolveCardTask(_context, (ActivateCardCommand)_command));
    }
}

