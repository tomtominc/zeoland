﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortBehaviour : MonoBehaviour
{
    private SpriteRenderer _renderer;

    private void Start()
    {
        _renderer = GetComponentInChildren < SpriteRenderer >();
    }

    private void Update()
    {
        _renderer.sortingOrder = -(int)(transform.position.y * 100);
    }
}
