﻿using UnityEngine.UI;
using UnityEngine;

public class StatText : MonoBehaviour
{
    public Color lowerColor;
    public Color higherColor;
    public StatArrow arrow;
    public Text statLabel;

    public void SetStat(int equipedStat, int newStat)
    {
        if (equipedStat > newStat)
        {
            statLabel.color = lowerColor;
            arrow.DoArrowEffect(StatArrow.State.Low);
        }
        else if (equipedStat < newStat)
        {
            statLabel.color = higherColor;
            arrow.DoArrowEffect(StatArrow.State.High);
        }
        else
        {
            statLabel.color = Color.white;
            arrow.DoArrowEffect(StatArrow.State.None);
        }

        statLabel.text = newStat.ToString();
    }
}
