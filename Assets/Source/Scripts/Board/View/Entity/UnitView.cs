﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// Base class for everything that moves on a grid.
/// This is used for characters / enemies / other 
/// </summary>
public class UnitView : MonoBehaviour
{
    public Transform mainTransform;
    [SerializeField]private Transform moveDopple1;
    [SerializeField]private Transform moveDopple2;
    [SerializeField]private Text powerLabel;
    [SerializeField]private Text healthLabel;
    [SerializeField]private Canvas statsCanvas;
    public UnitState unitState;

    private Color cellColor = Color.black;
    private float heightOffset = 16f;

    private SpriteRenderer mainRenderer;
    private SpriteAnimation mainAnimator;
    private SpriteAnimation moveDopple1Animator;
    private SpriteAnimation moveDopple2Animator;
    private SpriteRenderer moveDopple1Renderer;
    private SpriteRenderer moveDopple2Renderer;

    private SpriteOutline mainOutline;
    private Vector3 cellOffset;

    public Vector3 position
    {
        get { return transform.position; }
    }

    public enum Side
    {
        Up,
        Down,
        RightUp,
        RightDown,
        LeftUp,
        LeftDown
    }

    public enum AnimationState
    {
        IdleUp,
        IdleDown
    }

    [HideInInspector]
    public AnimationState currentAnimation;

    public void Initialize(UnitState unitState)
    {
        this.unitState = unitState;

        mainRenderer = mainTransform.GetComponent < SpriteRenderer >();
        mainAnimator = mainTransform.GetComponent < SpriteAnimation >();
        mainOutline = mainTransform.GetComponent < SpriteOutline >();

        moveDopple1Animator = moveDopple1.GetComponent < SpriteAnimation >();
        moveDopple2Animator = moveDopple2.GetComponent < SpriteAnimation >();

        moveDopple1Renderer = moveDopple1.GetComponent < SpriteRenderer >();
        moveDopple2Renderer = moveDopple2.GetComponent < SpriteRenderer >();

        mainAnimator.assets.Add(unitState.GetUnit().BoardAnimator);
        moveDopple1Animator.assets.Add(unitState.GetUnit().BoardAnimator);
        moveDopple2Animator.assets.Add(unitState.GetUnit().BoardAnimator);

        moveDopple1.gameObject.SetActive(false);
        moveDopple2.gameObject.SetActive(false);

        cellOffset = mainTransform.localPosition;

        mainAnimator.Play(unitState.GetUnit().Team == Team.Character ? 
            AnimationState.IdleUp : AnimationState.IdleDown);
    }

    public Sequence PlayDamageAnimation()
    {
        return HSVUtility.DamageWithInvincibility(mainRenderer, 1f, 2f, 6, null, null);
    }

    private void Update()
    {
        mainRenderer.sortingOrder = -(int)(transform.position.y * 100);
    }

    public virtual void Highlight(bool hightlight)
    {
        mainOutline.outline = hightlight;
        //statsCanvas.gameObject.SetActive(hightlight);
    }

    public virtual float GetSelectedHeightOffset()
    {
        return heightOffset;
    }

    public virtual void SetLookAtSide(Side side)
    {
        AnimationState animation = side == Side.RightUp || side == Side.LeftUp ?
            AnimationState.IdleUp : AnimationState.IdleDown;

        mainRenderer.flipX = (side == Side.RightUp || side == Side.RightDown);

        PlayAnimation(animation);
    }

    public virtual void PlayAnimation(AnimationState state)
    {
        if (currentAnimation != state)
        {
            currentAnimation = state;
            mainAnimator.Play(state);
        }
    }

    public void SetPower(int power)
    {
        powerLabel.text = power.ToString();
    }

    public void SetHealth(int health)
    {
        healthLabel.text = health.ToString();
    }

    public virtual IEnumerator Move(MoveCommand moveCommand)
    {
        // grab the cell that I'm on and the cell I'm moving to
        CellView fromView = BoardView.Instance.GetCell(moveCommand.fromPoint);
        CellView toView = BoardView.Instance.GetCell(moveCommand.toPoint);

        // tween the first dopple and then turn it off
        float duration = 0.2f;
        float timeBetweenScaler = 0f;
        Vector3 minScale = new Vector3(0.1f, 1.5f, 1f);

        // move the dopples into place and scale them back to normal size
        moveDopple1.localPosition = cellOffset;
        moveDopple2.position = toView.transform.position;
        moveDopple2.localPosition += cellOffset;

        moveDopple1.localScale = Vector3.one;
        moveDopple2.localScale = minScale;

        // set the first dopple on and the main sprite off
        moveDopple1.gameObject.SetActive(true);
        mainTransform.gameObject.SetActive(false);

        moveDopple1Renderer.flipX = mainRenderer.flipX;
        moveDopple1Animator.Play(currentAnimation);

        moveDopple1.DOScale(minScale, duration).OnComplete 
            (() => moveDopple1.gameObject.SetActive(false));

        yield return new WaitForSeconds(duration * timeBetweenScaler);

        moveDopple2.gameObject.SetActive(true);

        moveDopple1Renderer.flipX = mainRenderer.flipX;
        moveDopple1Animator.Play(currentAnimation);

        Tweener teleportIn = moveDopple2.DOScale(Vector3.one, duration).OnComplete
            (() => moveDopple2.gameObject.SetActive(false));
        
        yield return teleportIn.WaitForCompletion();

        transform.position = toView.transform.position;

        mainTransform.gameObject.SetActive(true);

        fromView.RemoveHighlight();
        toView.Highlight(unitState.GetUnit().Color);
    }
}
