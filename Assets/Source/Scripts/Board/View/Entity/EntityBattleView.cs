﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using DG.Tweening;

public class EntityBattleView : MonoBehaviour
{
    public enum EventType
    {
        Damage
    }

    [System.Serializable]
    public class AnimationEvent
    {
        public int frame;
        public EventType eventType;
    }

    public UnitState unitState;
    public Transform mainTransform;

    private SpriteRenderer spriteRenderer;
    private SpriteAnimation spriteAnimator;

    [Header("BATTLE PROPERTIES")]
    [SerializeField] private float attackMoveDistance = 1f;
    [SerializeField] private float attackMoveTime = 0.3f;
    [SerializeField] private Ease attackMoveEase;

    private EntityBattleView currentDefender;

    public GameObject effectPrefab;
    public Vector3 effectOffset;
    public List < AnimationEvent > animationEvents;

    public void Initialize(UnitState unitState)
    {
        this.unitState = unitState;

        spriteRenderer = mainTransform.GetComponent < SpriteRenderer >();
        spriteAnimator = mainTransform.GetComponent < SpriteAnimation >();

        spriteAnimator.assets.Clear();
        spriteAnimator.assets.Add(this.unitState.GetUnit().OverlayAnimator);
    }

    public void StartIdle()
    {
        spriteAnimator.Play("Idle");
    }

    public IEnumerator Attack(EntityBattleView defender)
    {
        bool isDone = false;
        currentDefender = defender;
        Vector3 originalPos = transform.position;
        float side = Mathf.Sign(currentDefender.transform.position.x - transform.position.x);

        spriteRenderer.sortingOrder = 5;

        spriteAnimator.PlayWithOnKeyFrameEvent("Attack", (anim, data, frame, id) =>
            {
                if (frame == data.frameDatas.Count - 1)
                {
                    isDone = true;
                    spriteAnimator.Play("Idle");
                }

                if (animationEvents.Count > 0)
                {
                    List <AnimationEvent> animEvents = animationEvents.FindAll(x => x.frame == frame);
                    DoEvents(animEvents);
                }
            });

        transform.DOMoveX(transform.position.x + (attackMoveDistance * side), attackMoveTime)
            .SetEase(attackMoveEase);

        while (!isDone)
            yield return null;

        Tweener attackTween = transform.DOMoveX(originalPos.x, attackMoveTime)
            .SetEase(attackMoveEase);
        
        yield return attackTween.WaitForCompletion();

        spriteRenderer.sortingOrder = 4;
    }

    public void DoEvents(List < AnimationEvent > animEvents)
    {
        if (animEvents != null && animEvents.Count > 0)
        {
            foreach (var animEvent in animEvents)
            {
                switch (animEvent.eventType)
                {
                    case EventType.Damage:
                        DamageEffect();
                        break;
                }
            }
        }
    }

    public IEnumerator Damage()
    {
        spriteRenderer.sortingOrder = 6;

        GameObject damageEffect = Instantiate < GameObject >(effectPrefab);
        damageEffect.transform.position = transform.position + effectOffset;

        SpriteAnimation damageAnimator = damageEffect.GetComponent < SpriteAnimation >();

        damageAnimator.PlayWithCallBack("Effect", (x, y) =>
            {
                Destroy(damageEffect); 
            });

        spriteAnimator.PlayWithCallBack("Hurt", (x, y) =>
            {
                spriteAnimator.Play("Idle"); 
            });
        
        yield return StartCoroutine(HSVUtility.Damage(spriteRenderer, 0.5f));
        spriteRenderer.sortingOrder = 4;

    }

    public void DamageEffect()
    {
        CameraShake.ShakeAll();

        if (currentDefender != null)
        {
            BattleView.Instance.Damage(unitState, currentDefender.unitState);
            StartCoroutine(currentDefender.Damage());
        }
    }
}
