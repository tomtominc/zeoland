﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;
using System.Linq;

public class BoardView : GridBehaviour < FlatHexPoint >
{
    private static BoardView instance;

    public static BoardView Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < BoardView >();

            return instance;
        }
    }

    private CellView lastHoveredCell;
   
   
    public FlatHexGrid < CellView > boardView;
    public FlatHexGrid < CellState > boardState;
    public bool initialized = false;
    public Color normalCellColor;

    public override void InitGrid()
    {
        initialized = true;
        boardView = (FlatHexGrid<CellView>)Grid.CastValues < CellView , FlatHexPoint >();
        boardState = (FlatHexGrid<CellState>)boardView.CloneStructure < CellState >();

        foreach (FlatHexPoint point in boardState)
        {
            CellState state = new CellState();
            boardState[point] = state;
            CellView cellView = GetCell(point);
            cellView.Initialize(state);
        }
    }

    public UnitView.Side GetSide(FlatHexPoint p0, FlatHexPoint p1)
    {
        UnitView.Side side = UnitView.Side.LeftDown;

        FlatHexPoint point = p1 - p0;


        point = new FlatHexPoint(point.X > 0 ? 1 : point.X < 0 ? -1 : 0,
            point.Y > 0 ? 1 : point.Y < 0 ? -1 : 0);

        if (point.Equals(FlatHexPoint.Zero))
            side = UnitView.Side.LeftUp;
        if (point.Equals(FlatHexPoint.South))
            side = UnitView.Side.LeftDown;
        if (point.Equals(FlatHexPoint.SouthEast))
            side = UnitView.Side.RightDown;
        if (point.Equals(FlatHexPoint.SouthWest))
            side = UnitView.Side.LeftDown;
        if (point.Equals(FlatHexPoint.North))
            side = UnitView.Side.LeftUp;
        if (point.Equals(FlatHexPoint.NorthEast))
            side = UnitView.Side.RightUp;
        if (point.Equals(FlatHexPoint.NorthWest))
            side = UnitView.Side.LeftUp;

        return side;
    }

    public CellView GetCellByMouse()
    {
        CellView cell = null;

        if (Grid.Contains(GridBuilder.MousePosition))
            cell = boardView[GridBuilder.MousePosition];
        
        return cell;
    }

    public FlatHexPoint GetPointByMouse()
    {
        return GridBuilder.MousePosition;
    }

    public CellView GetCell(FlatHexPoint point)
    {
        if (boardView.Contains(point))
            return boardView[point];

        return null;
    }

    public CellView GetCell(Vector3 worldPoint)
    {
        return GetCell(GetPoint(worldPoint));
    }

    public FlatHexPoint GetPoint(Vector3 worldPoint)
    {
        return Map[worldPoint];
    }

    public CellView GetCellWorldPosition(Vector3 worldPoint)
    {
        return GetCell(GetPoint(worldPoint));
    }

    public FlatHexPoint GetCurrentPoint(int gameId)
    {
        UnitView entityView = GameManager.Instance.GetUnitView(gameId);
        return GetPoint(entityView.transform.position);
    }

    public List < CellView > GetOccupiedCells()
    {
        return boardView.Values.ToList().FilterByOccupied();
    }

    public PointList < FlatHexPoint > GetPoints(FlatHexPoint point, int range, 
                                                RangeType rangeType, bool includePoint = true)
    {
        PointList < FlatHexPoint > pointList = new PointList < FlatHexPoint >();

        switch (rangeType)
        {
            case RangeType.Normal:
                pointList = Algorithms.GetPointsInRange(boardState, point, x => true, (x, y) => 1, range).ToPointList();
                break;
            case RangeType.Line:

                for (int i = 0; i < range + 1; i++)
                {
                    foreach (var direction in FlatHexPoint.MainDirections)
                    {
                        pointList.Add(point + (direction * i));
                    }
                }
                break;
            default:
                pointList = Algorithms.GetPointsInRange(boardState, point, x => true, (x, y) => 1, range).ToPointList();
                break;

        }

        return pointList;

    }

    public List < CellView > GetCellViews(FlatHexPoint point, int range, 
                                          RangeType rangeType, bool includePoint = true)
    {
        List < CellView > cellViews = new List < CellView >();
        PointList < FlatHexPoint > pointList = GetPoints(point, range, rangeType);

        foreach (var neighbor in pointList)
        {
            if (point.Equals(neighbor) && !includePoint)
                continue;

            CellView view = GetCell(neighbor);

            if (view == null)
                continue;
            
            cellViews.Add(view);
        }

        return cellViews;
    }

    public List < CellView > HighlightCellsByRange(FlatHexPoint point, int range, 
                                                   RangeType rangeType, Color color, bool includePoint = true)
    {
        List < CellView > cellViews = GetCellViews(point, range, rangeType, includePoint);

        cellViews.ForEach(x => x.Highlight(color));

        return cellViews;
    }

}
