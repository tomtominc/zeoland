﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ViewResolver : SingletonBehaviour < ViewResolver >
{
    private Queue < CommandQueue > queuedCommands;

    public bool isResolving { get; set; }

    private List < Action > finishedActions;

    private void Awake()
    {
        queuedCommands = new Queue<CommandQueue>();
        finishedActions = new List < Action >();
    }

    public void AddCommandQueue(CommandQueue commandQueue, Action finished = null)
    {
        commandQueue.Resolve();
        queuedCommands.Enqueue(commandQueue);
      
        if (finished != null)
            finishedActions.Add(finished);
        
        if (!isResolving)
            StartCoroutine(Resolve());
    }

    private IEnumerator Resolve()
    {
        isResolving = true;

        while (queuedCommands.Count > 0)
        {
            CommandQueue commandQueue = queuedCommands.Dequeue();

            while (commandQueue.commands.Count > 0)
            {
                Command command = commandQueue.commands.Dequeue();

                yield return StartCoroutine(ResolveCommand(command));
            }
        }

        isResolving = false;
        finishedActions.ForEach(action => action());
        finishedActions.Clear();
    }

    private IEnumerator ResolveCommand(Command command)
    {
        switch (command.commandType)
        {
            case CommandType.Summon:
                yield return StartCoroutine(ResolveCommandTask < SummonCommandTask >(command));
                yield break;
            case CommandType.Move:
                yield return StartCoroutine(ResolveCommandTask<MoveCommandTask>(command));
                yield break;
            case CommandType.Attack:
                yield return StartCoroutine(ResolveCommandTask<AttackCommandTask>(command));
                yield break;
            case CommandType.Draw:
                yield return StartCoroutine(ResolveCommandTask<DrawCommandTask>(command));
                yield break;
            case CommandType.DrawMultiple:
                yield return StartCoroutine(ResolveCommandTask<DrawMultipleCommandTask>(command));
                yield break;
            case CommandType.ActivateCard:
                yield return StartCoroutine(ResolveCommandTask<ActivateCardCommandTask>(command));
                yield break;
            default:
                Debug.LogWarningFormat("Command of Type {0} has not been implemented.", command.commandType);
                yield break;
        }
    }

    public IEnumerator ResolveCommandTask < T >(Command command)  where T : CommandTask, new()
    {
        T task = new T();
        task.Initialize(this, command);
        yield return StartCoroutine(task.Resolve());
    }
}
