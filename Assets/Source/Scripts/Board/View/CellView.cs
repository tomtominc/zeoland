﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;
using System.Linq;
using RutCreate.LightningDatabase;
using DG.Tweening;

public class CellView : SpriteCell
{
    public Color normalColor;
    public Color attackReadyColor;
    public Color hoverColor;

    public CellState cellState;

    private Tweener upAndDownTween;

    public Vector3 position
    {
        get { return transform.position; }
    }

    public void Initialize(CellState cellState)
    {
        this.cellState = cellState;
    }

    public void Highlight(Color color)
    {
        Color = color;
    }

    public void RemoveHighlight()
    {
        Color = normalColor;
    }

    public void AnimateUpAndDown()
    {
        transform.GetChild(0).DOLocalMoveY(0.1f, 0.1f).SetLoops(-1, LoopType.Yoyo);
        Highlight(Color.yellow);
    }

    public void DisableAnimations()
    {
        DOTween.Kill(transform.GetChild(0));
        RemoveHighlight();
    }
   
}

public static class CellViewFilters
{
    public static List < CellView > FilterByOccupied(this List < CellView > cellView)
    {
        return cellView.Where(x => x.cellState.IsOccupied()).ToList();
    }

    public static List < CellView > FilterByUnoccupied(this List < CellView > cellView, params CellView[] whiteList)
    {
        if (whiteList == null)
            return cellView.Where(x => !x.cellState.IsOccupied()).ToList();
        else
            return cellView.Where(x => !x.cellState.IsOccupied() || whiteList.Contains(x)).ToList();
           
    }
}
