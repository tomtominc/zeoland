﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TurnAssets
{
    public Team team;
    public Sprite label;
    public Sprite banner;

}

public class TurnView : MonoBehaviour
{
    public Image turnLabel;
    public Image turnBanner;

    public List < TurnAssets > _turnAssets;

    public void Set(Team team)
    {
        TurnAssets assets = _turnAssets.Find(x => x.team == team);

        turnLabel.sprite = assets.label;
        turnBanner.sprite = assets.banner;
    }
}
