﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StatArrow : MonoBehaviour
{
    public enum State
    {
        Inactive,
        None,
        High,
        Low
    }

    private Animator animator;
    private Tweener arrowTween;
    private Vector3 originalPosition;

    private State currentState;

    public void DoArrowEffect(State state)
    {
        if (animator == null)
            animator = GetComponentInChildren < Animator >();

        currentState = state;
        
        switch (currentState)
        {
            case State.None:
                animator.Play("ArrowDisabled");
                break;
            case State.High:
                animator.Play("ArrowUp");
                break;
            case State.Low:
                animator.Play("ArrowDown");
                break;
        }

    }

}
