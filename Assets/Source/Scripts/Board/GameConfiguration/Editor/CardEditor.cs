﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using UnityEditor;
using Framework;
using CCGKit;
using System.Linq;

partial class ConfigurationEditor
{
    // move these variables to card editor
    private ReorderableList currentEntityList;

    private Entity currentEntity;

    private Vector2 cardCollectionScrollPos;

    private void InitCardCollectionEditor()
    {
        currentEntityList = EditorUtils.SetupReorderableList("Entities", config.EntityCollection, 
            ref currentEntity, (rect, x) =>
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight), 
                    string.Format("{0} [{1}]", x.Name, x.EntityType)); 
            },
            (x) =>
            {
                currentEntity = x;
            },
            () =>
            {
                var menu = new GenericMenu();
                foreach (var entityType in Enum<EntityType>.ToValues ())
                    menu.AddItem(new GUIContent(entityType.ToString()), false, 
                        CreateEntityCallback, entityType);
                menu.ShowAsContext();
            },
            (x) =>
            {
                currentEntity = null;
            }
        );
    }

    private void CreateEntityCallback(object obj)
    {
        Entity entity = new Entity();

        if (config.EntityCollection.Count > 0)
        {
            var higherIdCard = config.EntityCollection.Aggregate((x, y) => x.Id > y.Id ? x : y);
            if (higherIdCard != null)
                entity.Id = higherIdCard.Id + 1;
        }
        else
        {
            entity.Id = 0;
        }

        entity.Name = "None";
        entity.EntityType = (EntityType)obj;

        config.EntityCollection.Add(entity);

        InitCardCollectionEditor();
    }


    private void DrawCardCollectionEditor()
    {
        var oldLabelWidth = EditorGUIUtility.labelWidth;
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 80;
        GUILayout.BeginVertical();

        if (currentEntityList != null)
        {
            GUILayout.BeginVertical(GUILayout.MaxWidth(250));
            cardCollectionScrollPos = GUILayout.BeginScrollView(cardCollectionScrollPos, false, false, GUILayout.Width(250), GUILayout.Height(600));
            if (currentEntityList != null)
                currentEntityList.DoLayoutList();
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }
        else
        {
            Debug.LogWarning("Current Card List is null");
        }

        GUILayout.EndVertical();

        EditorGUIUtility.labelWidth = oldLabelWidth;

        if (currentEntity != null)
            DrawEntity(currentEntity);
       
        GUILayout.EndHorizontal();
       
    }

    public static Color CardColor(EntityType type)
    {
        switch (type)
        {
            case EntityType.Unit:
                return EditorColor.blue;
            case EntityType.Card:
                return EditorColor.orange;
            case EntityType.Ability:
                return EditorColor.green;
            default:
                return Color.white;
        }
    }

    private void DrawEntity(Entity entity)
    {
        GUILayout.BeginVertical();

        EditorGUIUtility.labelWidth = 100;

        float maxWidth = 190f;
        EditorGUIExtensions.DrawHeader(string.Format("{0} [{1}]", entity.Name, entity.Id), 
            CardColor(entity.EntityType), GUILayout.MaxWidth(300));

        EditorGUIExtensions.BeginContent(Color.white, GUILayout.MaxWidth(300));

        EditorGUILayout.Space();
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Name");
        entity.Name = EditorGUILayout.TextField(entity.Name, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Key");
        entity.Key = EditorGUILayout.TextField(entity.Key, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        if (entity.EntityType == EntityType.Unit)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Character Type");
            entity.CharacterType = (UnitType)EditorGUILayout.EnumPopup(entity.CharacterType, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Type");
        entity.EntityType = (EntityType)EditorGUILayout.EnumPopup(entity.EntityType, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Element");
        entity.Element = (Element)EditorGUILayout.EnumPopup(entity.Element, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        if (entity.EntityType == EntityType.Unit)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Hit Points");
            entity.HitPoints = EditorGUILayout.IntField(entity.HitPoints, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Magic Points");
            entity.MagicPoints = EditorGUILayout.IntField(entity.MagicPoints, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Magic Power");
            entity.MagicPower = EditorGUILayout.IntField(entity.MagicPower, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Speed");
            entity.Speed = EditorGUILayout.IntField(entity.Speed, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();
        }
       
        if (entity.EntityType == EntityType.Card || entity.EntityType == EntityType.Ability)
        {
            
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Accuracy");
            entity.Accuracy = EditorGUILayout.IntField(entity.Accuracy, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Critical");
            entity.Critical = EditorGUILayout.IntField(entity.Critical, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Equip Flag");
            entity.equipFlag = (UnitType)EditorGUILayout.EnumPopup(entity.equipFlag, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Attack Type");
            entity.attackType = (AttackType)EditorGUILayout.EnumPopup(entity.attackType, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Range Type");
            entity.RangeType = (RangeType)EditorGUILayout.EnumPopup(entity.RangeType, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Range");
            entity.Range = EditorGUILayout.IntField(entity.Range, GUILayout.MaxWidth(maxWidth));
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Power");
        entity.Power = EditorGUILayout.IntField(entity.Power, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Defense");
        entity.Defense = EditorGUILayout.IntField(entity.Defense, GUILayout.MaxWidth(maxWidth));
        EditorGUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Description");
        EditorStyles.textField.wordWrap = true;
        entity.Description = EditorGUILayout.TextArea(entity.Description, GUILayout.MaxWidth(maxWidth),
            GUILayout.MaxHeight(100));
        EditorGUILayout.EndHorizontal();

        EditorGUIExtensions.EndContent();

        GUILayout.EndVertical();


    }

}
