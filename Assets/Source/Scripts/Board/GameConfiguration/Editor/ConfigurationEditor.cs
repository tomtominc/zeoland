﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Framework;
using UnityEditorInternal;
using System.Linq;
using CCGKit;

public partial class ConfigurationEditor : EditorWindow
{
    private GameConfiguration config;
    private bool isLoading = false;

    [MenuItem("Window/Config Editor")]
    private static void Init()
    {
        var window = GetWindow(typeof(ConfigurationEditor));
        window.titleContent = new GUIContent("Config Editor");
    }

    private void OnEnable()
    {
        config = new GameConfiguration();

        config.LoadGameConfiguration();

        InitCardCollectionEditor();
    }

    private void OnConfigurationLoaded()
    {
        isLoading = false;

        InitCardCollectionEditor();
    }

    private void OnGUI()
    {
        GUI.enabled = !isLoading;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Load", GUILayout.MaxWidth(60)))
        {
            config.LoadGameConfiguration();
        }


        if (GUILayout.Button("Save", GUILayout.MaxWidth(60)))
        {
            config.SaveGameConfiguration();
        }
       
        if (GUILayout.Button("Upload", GUILayout.MaxWidth(60)))
        {
            isLoading = true; 
            config.UploadGameConfiguration(null, OnConfigurationLoaded);
        }

        if (GUILayout.Button("Download", GUILayout.MaxWidth(60)))
        {
            isLoading = true; 
            config.DownloadGameConfiguration(null, OnConfigurationLoaded);
        }

        GUILayout.EndHorizontal();

        GUILayout.Space(5);

        if (config == null)
            return;

        DrawCardCollectionEditor();
    }

   
}
