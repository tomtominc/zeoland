﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using System;
using Framework;


#if UNITY_EDITOR
using UnityEditor;
#endif
public sealed class GameConfiguration
{
    public const string SERVER_PATH = "https://www.duorpg.com/super-robot/configuration";

    public string LOCAL_PATH { get { return Application.dataPath + "/Source/Resources/Configuration"; } }

    public string RUNTIME_PATH = "Configuration";

    /// <summary>
    /// To update the version pass
    /// the server path + update version string + the the version number
    /// in the request.
    /// </summary>
    public const string UPDATE_VERSION = "update-version";
    /// <summary>
    /// to requst the version just passs the 
    /// server path and the request version string
    /// </summary>
    public const string REQUEST_VERSION = "request-version";
    /// <summary>
    /// to update the file pass the update file string
    /// the version + file name + the json you want to override
    /// </summary>
    public const string UPDATE_FILE = "update-file";
    /// <summary>
    /// version + file name
    /// </summary>
    public const string REQUEST_FILE = "request-file";

    public ConfigurationProperties Properties = new ConfigurationProperties();

    public List<Entity> EntityCollection = new List<Entity>();


    #region LOCAL FILE SAVE AND LOAD

    public void LoadGameConfiguration()
    {
        var configurationPath = string.Format("{0}/version", RUNTIME_PATH);
        var configurationProperties = LoadJSONFileRunTime<ConfigurationProperties>(configurationPath);

        if (configurationProperties != null)
            Properties = configurationProperties;

        var enityCollectionPath = string.Format("{0}/entity-collection", RUNTIME_PATH);
        var entityCollection = LoadJSONFileRunTime<List<Entity>>(enityCollectionPath);

        if (entityCollection != null)
            EntityCollection = entityCollection;
        else
            EntityCollection = new List<Entity>();

    }


    private T LoadJSONFile<T>(string path) where T : class
    {
        if (File.Exists(path))
        {
            var file = new StreamReader(path);
            var fileContents = file.ReadToEnd();

            var data = JsonConvert.DeserializeObject<T>(fileContents);
            file.Close();
            return data;
        }
        return null;
    }



    private T LoadJSONFileRunTime<T>(string path) where T : class
    {
        TextAsset jsonFile = Resources.Load<TextAsset>(path);
        return JsonConvert.DeserializeObject<T>(jsonFile.text);
    }

    public void SaveGameConfiguration()
    {
#if UNITY_EDITOR
        SaveJSONFile(LOCAL_PATH + "/version.json", Properties);
        SaveJSONFile(LOCAL_PATH + "/entity-collection.json", EntityCollection);
        AssetDatabase.Refresh();
#endif
    }

    private void SaveJSONFile<T>(string path, T data) where T : class
    {
        var file = new StreamWriter(path);
        var json = JsonConvert.SerializeObject(data, Formatting.Indented);
        file.WriteLine(json);
        file.Close();
    }

    #endregion


    #region SERVER FILE UPLOAD AND DOWNLOAD

    public void DownloadGameConfiguration(string versionOverride, Action onComplete)
    {
        BattleSystem.Instance.StartCoroutine(DownloadGameConfigurationCo(versionOverride, onComplete));
    }

    public IEnumerator DownloadGameConfigurationCo(string versionOverride, Action onComplete)
    {
        // Get configuration properties ( just the version number now )
        // ==============================
        string configurationPropertiesPath = string.Format("{0}/{1}", SERVER_PATH, REQUEST_VERSION);

        yield return BattleSystem.Instance.StartCoroutine(DownloadJSONFile<ConfigurationProperties>(configurationPropertiesPath, (configurationProperties) =>
             {
                 if (configurationProperties != null)
                     Properties = configurationProperties;
             }));

        // Get the version number or get the override version number
        string version = string.IsNullOrEmpty(versionOverride) ? Properties.version : versionOverride;

        // Get all the card collections
        // ==============================
        string entityCollectionPath = string.Format("{0}/{1}/{2}/entity-collection.json", SERVER_PATH, REQUEST_FILE, version);

        yield return BattleSystem.Instance.StartCoroutine(DownloadJSONFile<List<Entity>>(entityCollectionPath, (entityCollection) =>
           {
               if (entityCollection != null)
                   EntityCollection = entityCollection;
           }));

        if (onComplete != null)
            onComplete();

    }

    private IEnumerator DownloadJSONFile<T>(string path, Action<T> onComplete)
    {
        WWW webrequest = new WWW(path);

        yield return webrequest;

        if (string.IsNullOrEmpty(webrequest.error))
        {
            T objectData = JsonConvert.DeserializeObject<T>(webrequest.text);

            if (onComplete != null)
                onComplete(objectData);
        }
        else
        {
            T objectData = default(T);

            if (onComplete != null)
                onComplete(objectData);

            Debug.LogWarningFormat("No Json File found at path: {0} Error: {1}", path, webrequest.error);
        }


    }

    public void UploadGameConfiguration(string versionOverride, Action onComplete)
    {
        BattleSystem.Instance.StartCoroutine(UploadGameConfigurationCo(versionOverride, onComplete));
    }

    private IEnumerator UploadGameConfigurationCo(string versionOverride, Action onComplete)
    {
        var version = string.IsNullOrEmpty(versionOverride) ? Properties.version : versionOverride;

        string cartridgeCollectionPath = string.Format("{0}/{1}/{2}/entity-collection.json", SERVER_PATH, UPDATE_FILE, version);
        yield return BattleSystem.Instance.StartCoroutine(UploadJSONFile(cartridgeCollectionPath, EntityCollection));

        if (onComplete != null)
            onComplete();
    }

    private IEnumerator UploadJSONFile<T>(string path, T objectData)
    {
        string data = JsonConvert.SerializeObject(objectData);
        string file = string.Format("{0}/{1}", path, data);

        WWW webrequest = new WWW(file);

        yield return webrequest;

        if (!string.IsNullOrEmpty(webrequest.error))
        {
            Debug.LogWarningFormat("Error trying to save: {0} path: {1}", webrequest.error, file);
        }
    }

    #endregion

}

public class ConfigurationProperties
{
    public string version = "v0.0.1";
}

