﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RutCreate.LightningDatabase;

public sealed class GameManager
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = new GameManager();

            return instance;
        }
    }

    public int currentUnitGameId = -1;
    public int currentCardGameId = -1;

    public Dictionary < int , UnitState > unitStates;
    public Dictionary < int , UnitView > unitViews;
    public Dictionary < int , CardState > cardStates;
    public Dictionary < int , CardView > cardViews;

    public GameManager()
    {
        unitStates = new Dictionary < int, UnitState >();
        unitViews = new Dictionary < int,UnitView >();
        cardStates = new Dictionary<int, CardState>();
        cardViews = new Dictionary < int , CardView >();
    }

    public CardState GetCurrentCardState()
    {
        int currentCardId = Player.Instance.hand.GetCurrentCardId();

        if (currentCardId < 0)
            return null;

        return GetCardState(currentCardId);
    }

    public CardState CreateCardState(int cardId)
    {
        currentCardGameId++;
        Card card = GetCard(cardId);
        CardState cardState = new CardState(cardId, currentCardGameId);
        cardStates.Add(currentCardGameId, cardState);
        return cardState;
    }

    public Card GetCard(int cardId)
    {
        return Database.CardDatabase.Find(cardId);
    }

    public CardState GetCardState(int gameId)
    {
        return cardStates[gameId];
    }

    public UnitState CreateUnitState(string unitName, Team team)
    {
        currentUnitGameId++;
        Unit unit = GetUnit(unitName);
        UnitState unitState = new UnitState(unit.ID, currentUnitGameId, team);
        unitStates.Add(currentUnitGameId, unitState);
        return unitState;
    }

    public UnitState GetUnitState(int gameId)
    {
        return unitStates[gameId];
    }

    public void SetUnitView(UnitView entityView, int gameId)
    {
        unitViews.Add(gameId, entityView);
    }

    public UnitView GetUnitView(int gameId)
    {
        return unitViews[gameId];
    }

    public Unit GetUnit(string unitName)
    {
        return Database.UnitDatabase.Find(unitName);
    }

    public Unit GetUnit(int id)
    {
        return Database.UnitDatabase.Find(id);
    }

    public List < UnitState > GetUnitsOfTeam(Team team)
    {
        return unitStates.Values.Where(x => x.Team == team).ToList();
    }
}
