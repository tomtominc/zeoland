﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : SingletonBehaviour<PrefabManager>
{
    public Transform unitPrefab;

    public static UnitView GetInstance(int gameId)
    {
        UnitState unitState = GameManager.Instance.GetUnitState(gameId);

        Transform clone = Instantiate<Transform>(Instance.unitPrefab);
        clone.name = unitState.GetUnit().Name;
        clone.SetParent(Instance.transform);

        UnitView unitView = clone.GetComponent<UnitView>();
        unitView.Initialize(unitState);

        GameManager.Instance.SetUnitView(unitView, gameId);
        return unitView;
    }
}
