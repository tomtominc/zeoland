﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AssetManager : MonoBehaviour
{
    private static AssetManager instance;

    public static AssetManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < AssetManager >();
            return instance;
        }
    }

    public List < Texture > spriteSheets;
    private Dictionary < string, Sprite > loadedSprites;

    private void Awake()
    {
        loadedSprites = new Dictionary < string, Sprite >();

        foreach (var texture in spriteSheets)
        {
            List < Sprite > sprites = Resources.LoadAll < Sprite >(texture.name).ToList();

            foreach (var sprite in sprites)
            {
                loadedSprites.Add(sprite.name, sprite);
            }
        }
       
    }

    public Sprite GetSprite(object key)
    {
        return loadedSprites[key.ToString()];
    }


}
