﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum BoardState
{
    TurnStartUpdate,
    TurnEndUpdate,
    BoardUpdate,
    UnitUpdate,
    CardUpdate,
    AttackUpdate,
    EnemyUpdate,
    WaitUpdate
}

public interface IBoardStateHandler
{
    void OnStateChanged(BoardState state);
}

public class BoardStateManager : SingletonBehaviour < BoardStateManager >
{
    public Texture2D cursorNormal;
    public Texture2D cursorClickable;
    public Texture2D cursorGrab;

    int cursorIndex;

    private List < IBoardStateHandler > stateHandlers;


    private void Start()
    {
        SetCursorNormal();

        stateHandlers = transform.parent.
            GetComponentsInChildren < IBoardStateHandler >().ToList();
    }

    public void ChangeState(BoardState state)
    {
        stateHandlers.ForEach(updater => updater.OnStateChanged(state));

    }

    private void Update()
    {
//        if (cursorIndex == 0)
//            Cursor.SetCursor(cursorNormal, Vector2.zero, CursorMode.Auto);
//        if (cursorIndex == 1)
//            Cursor.SetCursor(cursorClickable, Vector2.zero, CursorMode.Auto);
//        if (cursorIndex == 2)
//            Cursor.SetCursor(cursorGrab, Vector2.zero, CursorMode.Auto);
    }

    public static void SetCursorNormal()
    {
        Instance.cursorIndex = 0;
    }

    public static void SetCursorClickable()
    {
        Instance.cursorIndex = 1;
    }

    public static void SetCursorGrab()
    {
        Instance.cursorIndex = 2; 
    }
}
