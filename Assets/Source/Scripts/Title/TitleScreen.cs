﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Gamelogic.Grids.Examples;

public class TitleScreen : MonoBehaviour
{
    public float startupDelay = 0.1f;



    [HeaderAttribute("BACKGROUND")]
    public RectTransform background;
    public Ease backgroundMoveEase = Ease.Linear;
    public float backgroundInDuration;
    public Vector2 backgroundMoveInPosition;
    public float backgroundFadeToBlackDuration;

    [Header("POCKET SIGN")]
    public RectTransform pocket;
    public Vector2 pocketMovePosition;
    public float pocketMoveDuration;

    [HeaderAttribute("LOGO")]
    public Image logo;
    public Image logoOverlay;
    public float logoFadeToColorDuration;
    [Range(0f, 1f)]
    public float logoInsertPosition = 0.75f;

    [Header("EARRINGS")]
    public Image earring_0;
    public Image earring_1;
    public float earringFadeInTime;

    [Header("PRESS START")]
    public Image pressStart;
    public float pressStartFadeInTime;

    [Header("RIGHTS RESERVERED")]
    public Image rightsReserved;
    public float rightsReservedFadeInTime;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(startupDelay);

        Sequence sequence = DOTween.Sequence();
        // BACKGROUND
        // -- Moves down to zero position 
        // -- Fades to darker color
        sequence.Append(background.DOAnchorPos(backgroundMoveInPosition, backgroundInDuration).SetEase(backgroundMoveEase));
        sequence.Join(background.GetComponent<Image>().DOColor(Color.grey, backgroundFadeToBlackDuration));
        sequence.Join(pocket.DOAnchorPos(pocketMovePosition, pocketMoveDuration));


        // LOGO
        // -- Fades in pure white
        // -- Fades to color
        // -- Moves to position
        sequence.InsertCallback(backgroundInDuration * logoInsertPosition, () =>
            {
                logo.gameObject.SetActive(true); 
            });
        sequence.Join(logoOverlay.DOFade(0f, logoFadeToColorDuration));

        // EARRINGS
        sequence.AppendCallback(() =>
            {
                earring_0.gameObject.SetActive(true);
                earring_1.gameObject.SetActive(true);
            });
        sequence.Join(earring_0.transform.GetChild(0).GetComponent < Image >().DOFade(0f, earringFadeInTime));
        sequence.Join(earring_1.transform.GetChild(0).GetComponent < Image >().DOFade(0f, earringFadeInTime));

        sequence.AppendCallback(() =>
            {
                pressStart.rectTransform.DOAnchorPosY(22f, 0.8f, true).SetLoops(-1, LoopType.Yoyo); 
            });
        sequence.Append(pressStart.DOFade(1f, pressStartFadeInTime));
        sequence.Join(rightsReserved.DOFade(1f, rightsReservedFadeInTime));


    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            TransitionCamera.Instance.Transition("FadeToWhite", "FadeToWhite", "Overworld");
        }
    }
}
