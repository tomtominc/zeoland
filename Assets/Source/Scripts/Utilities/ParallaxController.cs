﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class ParallaxController : MonoBehaviour
{
    [SerializeField]private bool _moveSelf = false;
    [SerializeField]private Vector2 _speed = new Vector2(3f, 0f);
    [SerializeField]private float velocity = 2f;
    [SerializeField]private Vector2 _direction = new Vector2(-1f, 0f);

    private List < ParallaxLayer > layers;

    private void Start()
    {
        layers = GetComponentsInChildren < ParallaxLayer >().ToList();    
        layers = layers.OrderBy(x => x.layerDepth).ToList();
    }

    private Vector2 GetSpeed(int depth)
    {
        return new Vector2(_speed.x + (depth * velocity), _speed.y + (depth * velocity));
    }

    private void Update()
    {
        if (_moveSelf)
            Move(_speed, _direction);
    }

    public void Move(Vector2 speed, Vector2 direction)
    {
        _speed = speed;
        _direction = direction;

        layers.ForEach(x => x.Move(GetSpeed(x.layerDepth), _direction));
    }
}
