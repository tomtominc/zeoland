﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class ScrollableRawImage : MonoBehaviour
{

    public float speed;
    public Vector2 direction;

    private RawImage rawImage;

    private void Start()
    {
        rawImage = GetComponent < RawImage >();    
    }

    private void Update()
    {
        Rect rect = rawImage.uvRect;
        rect.position += direction * speed * Time.deltaTime;
        rawImage.uvRect = rect;
    }
}

