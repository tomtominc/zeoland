﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class ParallaxLayer : MonoBehaviour
{
    public int layerDepth = 0;
    
    [SerializeField]private bool _moveSelf = true;
    [SerializeField]private Camera _camera;
    [SerializeField]private Vector2 _speed = new Vector2(10f, 0f);
    [SerializeField]private Vector2 _direction = new Vector2(-1f, 0f);

    private List < SpriteRenderer > layerParts;

    private void Start()
    {
        layerParts = GetComponentsInChildren < SpriteRenderer >().ToList();
        layerParts = layerParts.OrderBy(x => x.transform.position.x).ToList();
    }

    private void Update()
    {
        if (_moveSelf)
            Move(_speed, _direction);
    }

    public void UpdateParallax()
    {
        SpriteRenderer firstChild = layerParts.FirstOrDefault();

        if (firstChild.transform.position.x < _camera.transform.position.x)
        {
            if (firstChild.IsVisibleFrom(_camera) == false)
            {
                SpriteRenderer lastChild = layerParts.LastOrDefault();
                Vector2 lastPosition = lastChild.transform.position;
                Vector3 lastSize = (lastChild.bounds.max - lastChild.bounds.min);

                firstChild.transform.position = new Vector2(lastPosition.x + lastSize.x, firstChild.transform.position.y);

                layerParts.Remove(firstChild);
                layerParts.Add(firstChild);
            }
        }
    }

    public void Move(Vector2 speed, Vector2 direction)
    {
        Vector2 movement = new Vector2(speed.x * direction.x, speed.y * direction.y);

        movement *= Time.deltaTime;

        transform.Translate(movement);

        UpdateParallax();
    }

}
