﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

public class CardInputState : InputState
{
    private CardState cardState;

    public override void OnStateStart()
    {
        cardState = GameManager.Instance.GetCurrentCardState();
        PlayerHandGUI.triggerCard = false;
    }

    public override void OnStateEnd()
    {
        BoardView.Instance.HighlightCellsByRange 
        (
            BoardView.Instance.GetPointByMouse(), 
            cardState.GetRange(), 
            RangeType.Normal,
            BoardView.Instance.normalCellColor
        );

        PlayerHandGUI.triggerCard = false;
    }

    public override void OnMouseEnter(Transform transform)
    {
        CellView cellView = transform.GetComponent < CellView >();

        if (cellView != null)
        {
            PlayerHandGUI.triggerCard = true;
            PlayerHandGUI.triggeredPoint = BoardView.Instance.GetPoint(cellView.position);
            BoardView.Instance.HighlightCellsByRange 
            (
                BoardView.Instance.GetPoint(cellView.position), 
                cardState.GetRange(), 
                RangeType.Normal,
                cardState.GetCard().Color
            );
        }
    }

    public override void OnMouseExit(Transform transform)
    {
        CellView cellView = transform.GetComponent < CellView >();

        if (cellView != null)
        {
            PlayerHandGUI.triggerCard = false;
            
            BoardView.Instance.HighlightCellsByRange
            (
                BoardView.Instance.GetPoint(cellView.position), 
                cardState.GetRange(), 
                RangeType.Normal,
                cellView.normalColor
            );
        }
    }

}
