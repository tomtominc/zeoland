﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputState : MonoBehaviour
{
    protected InputStateHandler handler;
    public string stateName;

    public virtual void Init(InputStateHandler handler)
    {
        this.handler = handler;
    }

    public virtual void OnStateStart()
    {
        
    }

    public virtual void OnStateUpdate()
    {
        
    }

    public virtual void OnStateEnd()
    {
        
    }

    public virtual void OnMouseExit(Transform transform)
    {
        
    }

    public virtual void OnMouseEnter(Transform transform)
    {
        
    }

    public virtual void OnMouseClick(Transform transform)
    {
        
    }

    public virtual void OnMouseRelease(Transform transform)
    {
        
    }
}
