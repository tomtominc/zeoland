﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using RutCreate.LightningDatabase;

/// <summary>
/// Handles input states, zeoland uses the mouse to control everything
/// so this class handles events like mouse over / mouse down / mouse up
/// and provides informoation to the different states that are attached to it.
/// </summary>
public class InputStateHandler : MonoBehaviour, IBoardStateHandler
{
   

    protected Dictionary < string, InputState > inputStates;

    internal Transform lastHit;
    internal BoardView boardView;
    internal string currentState = "none";

    internal int unitGameId = -1;

    internal UnitState currentUnit
    {
        get { return GameManager.Instance.GetUnitState(unitGameId); }
    }

    internal UnitView currentUnitView
    {
        get { return GameManager.Instance.GetUnitView(unitGameId); }
    }

    private void Awake()
    {
        inputStates = GetComponentsInChildren < InputState >().ToDictionary(x => x.stateName, x => x);

        foreach (var pair in inputStates)
            pair.Value.Init(this);
    }

    private void Start()
    {
        boardView = BoardView.Instance;
    }

    private void Update()
    {
        if (inputStates.ContainsKey(currentState))
            inputStates[currentState].OnStateUpdate();
        
        CellView cellView = boardView.GetCellByMouse();

        if (!cellView && !lastHit)
            return;

        if (!cellView)
        {
            if (inputStates.ContainsKey(currentState) && lastHit)
                inputStates[currentState].OnMouseExit(lastHit);

            lastHit = null;
            return;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            if (inputStates.ContainsKey(currentState))
                inputStates[currentState].OnMouseClick(cellView.transform);
        }

        if (lastHit == null || !lastHit.Equals(cellView.transform))
        {
            if (inputStates.ContainsKey(currentState) && lastHit)
                inputStates[currentState].OnMouseExit(lastHit);

            lastHit = cellView.transform;

            if (inputStates.ContainsKey(currentState) && lastHit)
                inputStates[currentState].OnMouseEnter(lastHit);
        }
    }

    public void ChangeState(object state)
    {
        if (inputStates.ContainsKey(currentState))
            inputStates[currentState].OnStateEnd();

        currentState = state.ToString();

        if (inputStates.ContainsKey(currentState))
            inputStates[currentState].OnStateStart();
    }

    public void SetCurrentUnitId(int gameId)
    {
        unitGameId = gameId;
    }

    public void OnStateChanged(BoardState state)
    {
        ChangeState(state);
    }

   

}
