﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardInputState : InputState
{
    public Color highlightColor = Color.white;

    public override void OnMouseEnter(Transform transform)
    {
        CellView cellView = transform.GetComponent < CellView >();

        if (cellView != null)
            ApplyHighlight(cellView);
    }

    public override void OnMouseClick(Transform transform)
    {
        CellView cellView = transform.GetComponent < CellView >();

        if (cellView != null)
            HandleCellClicked(cellView);
    }

    public override void OnMouseExit(Transform transform)
    {
        CellView cellView = transform.GetComponent < CellView >();

        if (cellView != null)
            RemoveHighlight(cellView);
        
    }

    public void HandleCellClicked(CellView cellView)
    {
        bool occupied = cellView.cellState.IsOccupied();

        if (occupied)
        {
            handler.SetCurrentUnitId(cellView.cellState.occupiedId);
            BoardStateManager.Instance.ChangeState(BoardState.UnitUpdate);
        }
    }

    public void ApplyHighlight(CellView cellView)
    {
        bool occupied = cellView.cellState.IsOccupied();

        if (occupied)
        {
            UnitState unitState = GameManager.Instance.GetUnitState(cellView.cellState.occupiedId);
            UnitView unitView = GameManager.Instance.GetUnitView(cellView.cellState.occupiedId);



            unitView.Highlight(true);
            BoardView.Instance.HighlightCellsByRange 
            (
                BoardView.Instance.GetPoint(cellView.position), 
                unitState.GetRange(), 
                RangeType.Normal,
                unitState.GetUnit().Color
            );
        }
        else
        {
            cellView.Highlight(highlightColor);
        }
    }

    public void RemoveHighlight(CellView cellView)
    {
        bool occupied = cellView.cellState.IsOccupied();

        if (occupied)
        {
            UnitState unitState = GameManager.Instance.GetUnitState(cellView.cellState.occupiedId);
            UnitView unitView = GameManager.Instance.GetUnitView(cellView.cellState.occupiedId);

            unitView.Highlight(false);

            BoardView.Instance.HighlightCellsByRange 
            (
                BoardView.Instance.GetPoint(cellView.position), 
                unitState.GetRange(), 
                RangeType.Normal,
                cellView.normalColor,
                false
            );
        }
        else
        {
            cellView.RemoveHighlight();
        }
    }
}
