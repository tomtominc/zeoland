﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using Gamelogic.Grids.Examples;
using System;

public class UnitInputState : InputState
{
    public TargetStatsMenu targetStatsMenu;

    private List < CellView > activatedCells;
    private UnitState unitState;
    private UnitView unitView;
    private UnitView targetUnit;

    public override void OnStateStart()
    {
        unitState = handler.currentUnit;
        unitView = handler.currentUnitView;

        activatedCells = BoardView.Instance.HighlightCellsByRange 
        (
            BoardView.Instance.GetPoint(unitView.position), 
            unitState.GetRange(), 
            RangeType.Normal,
            unitState.GetUnit().Color,
            false
        );

    }

    public override void OnStateUpdate()
    {
        if (targetUnit != null)
        {
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(targetUnit.position);
            RectTransform screenRect = (RectTransform)UIManager.GetUiContainer;

            Vector2 midScreen = new Vector2(Screen.width / 2f, Screen.height / 2f);
            Vector2 targetStatsMenuPos = Vector2.zero;

            float yOffset = 2;

            targetStatsMenuPos.x = screenPoint.x > midScreen.x ? 
                -(screenRect.sizeDelta.x * 0.25f) : screenRect.sizeDelta.x * 0.25f;
            targetStatsMenuPos.y = screenPoint.y > midScreen.y ? 
                -(screenRect.sizeDelta.y * 0.25f) - yOffset : (screenRect.sizeDelta.y * 0.25f) + yOffset;

            targetStatsMenu.GetComponent<RectTransform>().anchoredPosition = targetStatsMenuPos;

            targetStatsMenu.SetStats(unitState, targetUnit.unitState);
        }
    }


    public override void OnMouseEnter(Transform transform)
    {
        CellView cellView = transform.GetComponent<CellView>();

        if (cellView != null && activatedCells.Contains(cellView))
        {
            //var p0 = BoardView.Instance.GetPoint(unitView.position);
            //var p1 = BoardView.Instance.GetPoint(cellView.position);

            //unitView.SetLookAtSide(BoardView.Instance.GetSide(p0, p1));

            if (cellView.cellState.IsOccupied())
            {
                targetStatsMenu.gameObject.SetActive(true);

                targetUnit = GameManager.Instance.GetUnitView(cellView.cellState.occupiedId);
                targetUnit.Highlight(true);
            }
        }
    }

    public override void OnMouseExit(Transform transform)
    {
        CellView cellView = transform.GetComponent<CellView>();

        if (cellView != null && activatedCells.Contains(cellView))
        {
            if (cellView.cellState.IsOccupied())
            {
                targetStatsMenu.gameObject.SetActive(false);
                targetUnit = null;
                UnitView targetUnitView = GameManager.Instance.GetUnitView(cellView.cellState.occupiedId);
                targetUnitView.Highlight(false);
            }
        }
    }

    public override void OnMouseClick(Transform transform)
    {
        CellView cellView = transform.GetComponent<CellView>();

        if (cellView != null && activatedCells.Contains(cellView))
        {
            ResolveMouseClick(cellView);
        }
    }

    private void ResolveMouseClick(CellView cellView)
    {
        bool occupied = cellView.cellState.IsOccupied();


        targetStatsMenu.gameObject.SetActive(false);

        BoardView.Instance.HighlightCellsByRange 
        (
            BoardView.Instance.GetPoint(unitView.position), 
            unitState.GetRange(), 
            RangeType.Normal,
            cellView.normalColor,
            false
        );

        if (occupied)
            Attack(cellView);
        else
            Move(cellView);
    }

    private void Attack(CellView cellView)
    {
        PlayerHandGUI.Instance.AnimateOut();

        BattleView.Instance.OnBattleEnd += OnBattleEnd;
        CommandQueue command = new CommandQueue();
        var point = BoardView.Instance.GetPoint(targetUnit.position);
        command.DoCommand(unitState, point, CommandType.Attack);
        ViewResolver.Instance.AddCommandQueue(command);

        BoardStateManager.Instance.ChangeState(BoardState.BoardUpdate);
    }

    private void Move(CellView cellView)
    {
        CommandQueue command = new CommandQueue();
        var point = BoardView.Instance.GetPoint(cellView.position);
        command.DoCommand(unitState, point, CommandType.Move);
        ViewResolver.Instance.AddCommandQueue(command);

        BoardStateManager.Instance.ChangeState(BoardState.BoardUpdate);
    }

    private void OnBattleEnd(object sender, EventArgs e)
    {
        BattleView.Instance.OnBattleEnd -= OnBattleEnd;
        PlayerHandGUI.Instance.AnimateIn();
       
    }
}
