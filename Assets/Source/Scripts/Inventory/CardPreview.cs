﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RutCreate.LightningDatabase;

public class CardPreview : MonoBehaviour
{
    public Animator unitAnimator;
    public GameObject noPreviewLabel;
    public List < Image > fxImages;
    public Sprite emptySprite;

    public Text descriptionLabel;
    public Image element;

    [System.Serializable]
    public class ControllerDictionary : SerializableDictionary<string,RuntimeAnimatorController>
    {

    }

    public ControllerDictionary previewControllers;

    public void Refresh(Card card)
    {
        descriptionLabel.text = card.Description;
        element.sprite = Database.IconDatabase.Find(card.Element.ToString()).Sprite;

        if (previewControllers.ContainsKey(card.Name))
        {
            noPreviewLabel.gameObject.SetActive(false);
            unitAnimator.gameObject.SetActive(true);

            fxImages.ForEach(x => x.sprite = emptySprite);
            unitAnimator.runtimeAnimatorController = previewControllers[card.Name];
            unitAnimator.Play("Preview");

          
        }
        else
        {
            noPreviewLabel.gameObject.SetActive(true);
            unitAnimator.gameObject.SetActive(false);
        }
    }
}
