﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AlpacaSound.RetroPixelPro;

[RequireComponent(typeof(RetroPixelPro))]
public class ColorPalletteSwapper : MonoBehaviour
{
    public List < Colormap > colormaps;

    private int current = 0;
    private RetroPixelPro palleteEffect;

    private void Start()
    {
        palleteEffect = GetComponent < RetroPixelPro >();
        palleteEffect.colormap = colormaps[current];
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            current = current.Wrap(1, colormaps.Count);
            palleteEffect.colormap = colormaps[current];
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            current = current.Wrap(-1, colormaps.Count);
            palleteEffect.colormap = colormaps[current];
        }
            
    }
}
